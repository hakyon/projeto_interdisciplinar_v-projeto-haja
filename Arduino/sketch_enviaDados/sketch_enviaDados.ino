 #include <DallasTemperature.h>
 #include <OneWire.h>
 
  //int sensor1 = random(5, 120); //velocidade
  int sensor1 = 0;
  int sensor2 = random(5, 120); //umidade
  int sensor3 = random(5, 120); //temperatura 
  int sensor4 = random(0, 1); // n voltas
  int sensor5 = random(0, 1); // status de parado ou movimento 0 ou 1 

#define DS18B20 8 //DEFINE O PINO DO SENSOR DS18B20
OneWire ourWire(DS18B20);//CONFIGURA UMA INSTÂNCIA ONEWIRE PARA SE COMUNICAR COM DS18B20
DallasTemperature s_temp1(&ourWire); //PASSA A TEMPERATURA PARA O DallasTemperature

void setup() {
  randomSeed(analogRead(0));
  Serial.begin(9600);
  pinMode(13, OUTPUT); 
  s_temp1.begin(); // INICIA O SENSOR DS18B20	

}

void loop() {
  s_temp1.requestTemperatures();//REQUISITA A TEMPERATURA DO SENSOR 
  sensor1 = s_temp1 .getTempCByIndex(0);
  //sensor1 = random(5, 120); //velocidade
  sensor2 = random(5, 120); //umidade
  sensor3 = random(5, 120); //temperatura 
  sensor4 = random(0, 1); // n voltas
  sensor5 = random(0, 1); // status de parado ou movimento 0 ou 1 

  String sen_h1 =  String(sensor1, HEX);
  String sen_h2 =  String(sensor2, HEX);
  String sen_h3 =  String(sensor3, HEX);
  String sen_h4 =  String(sensor4, HEX);
  String sen_h5 =  String(sensor5, HEX);

  String leitura =  String("0x"+ sen_h1 + ","+  
                         "0x"+ sen_h2 + ","+ 
                         "0x"+ sen_h3 + ","+
                         "0x"+ sen_h4 + ","+
                         "0x"+ sen_h5 + "\0");
  

  Serial.println(leitura);
  Serial.flush();
  delay(2000);
  //flo = !flo;

  sensor1 = 0;
  sensor2 = 0;
  sensor3 = 0;
  sensor4 = 0;         
  sensor5 = 0;

}
