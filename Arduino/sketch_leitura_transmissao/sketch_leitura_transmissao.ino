/*******************************************************************/
/*Autor: Alan Azevedo Bancks                                       */
/*Autor: João Lucas Della Coletta Frangella                        */ 
/*Autor: Alexandre de Paula Guimarães                              */
/*Projeto: Arduino/Sensores - leitura de dados e transmissao       */
/*Data: 15/10/2017                                                 */
/*******************************************************************/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <DHT.h>
#include <dht.h>
#include <Adafruit_Sensor.h>
#include <SimpleDHT.h>


// befinicao de barramento
#define REFRESH_TIME 1000 //REED
#define ONE_WIRE_BUS 24
#define DHTTYPE DHT11 // DHT 11

//Pinagem
#define PORTA_SENSOR_DHT           A1 //sensor de temperatura/umidade
#define PORTA_LED_VERMELHO         5  //acionar led vermelho
#define PORTA_LED_AMARELO          6  //acionar led amarelo
#define PORTA_LED_VERDE            7  //acionar led verde
#define PORTA_CONTROLE             8  //sensor de controle de liga/desliga
#define PORTA_SENSOR_FREIO         9  //sensor de contagem de freios
#define PORTA_SENSOR_VOLTA         11 //sensor de contagem de voltas
#define PORTA_SENSOR_VELOCIDADE    12 //REED no digital 12


int sensor_gas_mq7 = analogRead(7);

//tabela de chacagem de comandos para execucao
#define COMANDO_SINAL_VERDE "0x0001"
#define COMANDO_SINAL_VERMELHO "0x0010"
#define COMANDO_SINAL_AMARELO "0x0011"
#define COMANDO_LIGAR "0x0100"
#define COMANDO_DESLIGAR "0x0101"
 

float tempMin = 999;
float tempMax = 0;
String buf;


//Variaveis para controle do estado dos leds
int estado_verm = 0;
int estado_amar = 0;
int estado_verd = 0;
int estado_controle = 0;
const int AOUTpin=0;//the AOUT pin of the CO sensor goes into analog pin A0 of the arduino
int menor_valor_lido=150;
int maior_valor_lido=380;
int contador_freio = 0; //freio
boolean freio  = 0; //freio
int contador_volta = 0; //VOLTA
boolean volta = 0; //VOLTA
long lastRefresh;   // Para trabalhar com o millis(), use long, e não float. //REED
float circMedida=0.16; //REED CIRC
//float raio=0.16;

int scan_velocidade = 0; //leitura VELOCIDADE
long contvolta = 0;   // Contador de giros //reed
int retorno_comando_recebido = 0;

//inicializacao de sensores
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature ds18B20(&oneWire);
DeviceAddress endereco_sensor_ds18B20;
DHT dht(PORTA_SENSOR_DHT, DHTTYPE);
LiquidCrystal lcd(0x3F,2,1,0,4,5,6,7,3, POSITIVE);

void setup() 
{
  
  Serial.begin(9600);
  dht.begin();
  ds18B20.begin();
  lcd.begin (20,4);

  pinMode(PORTA_LED_VERMELHO, OUTPUT);
  pinMode(PORTA_LED_AMARELO, OUTPUT);
  pinMode(PORTA_LED_VERDE, OUTPUT);
  pinMode(PORTA_CONTROLE, OUTPUT);
  
  digitalWrite(PORTA_CONTROLE, 0); // inicializa desligado
  
  pinMode(PORTA_SENSOR_FREIO, INPUT_PULLUP); //freio
  pinMode(PORTA_SENSOR_VELOCIDADE, INPUT_PULLUP); //reed
  attachInterrupt(0, calcula_velocidade, RISING); //reed 
     
  contvolta = 0; //reed
  lastRefresh = millis(); //reed
  scan_velocidade = 0; //reed
  pinMode(PORTA_SENSOR_VOLTA, INPUT_PULLUP);

  lcd.print("Equipe - Predadores");
  lcd.print("Inicializado");
}

void loop() {
  
  //primeiro verifica se existem comandos enviados pelo box
  while(Serial.available() > 0)
  {
      buf = Serial.read();
      retorno_comando_recebido = verifica_comando_led(buf);
      if(retorno_comando_recebido == 1)
      {
        lcd.print("Comando Executado do Box");
      }
       
  }

  //leitura de dados temperatura e umidade
  float scan_umidade = dht.readHumidity();
  float scan_temperatura = dht.readTemperature();
  ds18B20.requestTemperatures();
  float scan_temperatura_motor = ds18B20.getTempC(endereco_sensor_ds18B20);
  
  leitura_sensor_gas();
  leitura_contagem_voltas();
  leitura_velocidade();
  leitura_freio(); 
  //imprimir_dados_tela();
  
  
  
  //enviar em HEXA para a serial
  String umidade = String(int(scan_umidade),HEX);
  String temperatura = String(int(scan_temperatura), HEX);
  String temperatura_motor = String(scan_temperatura_motor, HEX);
  String nivel_C0 = String(sensor_gas_mq7,HEX);
  String ocorrencia_freio = String (contador_freio, HEX);
  String velocidade = String(scan_velocidade,HEX);
  String n_volta = String(contador_volta, HEX);

  String estado_led_verde = String (estado_verd, HEX);
  String estado_led_vermelho = String (estado_verm, HEX);
  String estado_led_amarelo = String (estado_amar, HEX);

  imprime_lcd(); // essa funcao somente imprime dados no LCD
    
  String leitura = String ("0x"+ velocidade + ","+  
                         "0x"+ umidade + ","+ 
                         "0x"+ temperatura + ","+
                         "0x"+ nivel_C0 + ","+
                         "0x"+ ocorrencia_freio +","+
                         "0x"+ n_volta + "," +
                         "0x"+ estado_led_verde + "," +
                         "0x"+ estado_led_amarelo + "," +
                         "0x"+ estado_led_vermelho + "\0");
                                            
  Serial.println (leitura);
  Serial.flush();
  delay(2000);

}

// por enquanto nao senso usado
void mostra_endereco_sensor(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // Adiciona zeros se necessario
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}


void calcula_velocidade()
{
   contvolta++;
}

int verifica_comando_led(String buffer) 
{
  if (buffer == COMANDO_SINAL_VERMELHO)
  {
      estado_verm = !estado_verm;
      digitalWrite(PORTA_LED_VERMELHO, estado_verm);
      return 1;
  }   
  
  if (buffer == COMANDO_SINAL_AMARELO)
  {
      estado_amar = !estado_amar;
      digitalWrite(PORTA_LED_AMARELO, estado_amar);
      return 1;   
  }
    
  if (buffer == COMANDO_SINAL_VERDE)
  {
      estado_verd = !estado_verd;
      digitalWrite(PORTA_LED_VERDE, estado_verd);
      return 1;
  } 
    
  if (buffer == COMANDO_LIGAR) // ligar veiculo
  {
      estado_controle = 1;
      digitalWrite(PORTA_CONTROLE,estado_controle);
      return 1;
  }

  if (buffer == COMANDO_DESLIGAR) // desliga o veiculo
  {
      estado_controle = 0;
      digitalWrite(PORTA_CONTROLE,estado_controle);
      return 1;
  }

  return 0;
}

//essa funcao imprimi tanto dados na tela como no proprio LCD
void imprimir_dados_tela()
{
  //dados temperatura e umidade 
  dht.read(PORTA_SENSOR_DHT); //
  Serial.print("Umidade = ");
  Serial.print(dht.readHumidity());
  Serial.println(" %  ");
  Serial.print("Temp ext = ");
  Serial.print(dht.readTemperature()); 
  Serial.println(" C ");
  lcd.setCursor(0,0); //SET LINHA LCD
  lcd.print("IN: ");//printa LCD
  lcd.print(dht.readTemperature());
  lcd.print("C ");//printa LCD
  lcd.print(dht.readHumidity());
  lcd.print("%");//printa LCD

  //dados temperatura motor
  Serial.print("Motor: ");
  Serial.print(ds18B20.getTempC(endereco_sensor_ds18B20));
  Serial.println(" C ");   
  lcd.setCursor(0,1); //SET LINHA LCD
  lcd.print("Motor: ");//printa LCD
  lcd.print(ds18B20.getTempC(endereco_sensor_ds18B20));
  lcd.print(" C");

  //dados sensor de gas
  Serial.print("CO: ");
  Serial.print(sensor_gas_mq7);
  Serial.println(" %");
  lcd.setCursor(0,2);
  lcd.print("CO: ");
  lcd.print(sensor_gas_mq7);
  lcd.print("% ");

  //dados freio
  Serial.print("Freio: "); //freio
  Serial.print(contador_freio); //freio
  Serial.println(" acionamentos"); //freio
  lcd.print("Freio: "); //freio
  lcd.print(contador_freio); //freio


  //dados velocidade
  lcd.setCursor(0,3);//REED
  lcd.print(int(scan_velocidade));//REED
  Serial.print(int(scan_velocidade));//REED
  lcd.print(" km/h ");//REED
  Serial.println("Km/h  " );//REED

  //contagem de voltas
  Serial.print(contador_volta); //VOLTAS
  Serial.println(" voltas"); //VOLTAS
  lcd.print("Voltas: "); //VOLTAS
  lcd.print(contador_volta); //VOLTAS

}

void ajuste_de_limites_mq7(int mq7) //Si  mples ajuste automÃ¡tico do mÃ¡ximos e mÃ­nimos
{ 
  if(mq7> maior_valor_lido)
  {
      maior_valor_lido=mq7;
  }
  if(mq7< menor_valor_lido)
  {
      menor_valor_lido= mq7;    
  }
  
}

void leitura_freio ()
{
   if(digitalRead(PORTA_SENSOR_FREIO)) 
      freio = 1; //freio

   if(!digitalRead(PORTA_SENSOR_FREIO) && freio) //freio
   { //freio
      freio = 0; //freio
      contador_freio+=1; //freio
   } 

}
void leitura_contagem_voltas()
{
   if(digitalRead(PORTA_SENSOR_VOLTA)) 
       volta = 1;

   if(!digitalRead(PORTA_SENSOR_VOLTA) && volta)
   {
       volta = 0;
       contador_volta+=1;    
   }
  
}

// leitura de sensor de velocidade
void leitura_velocidade()
{
  if (millis() - lastRefresh >= REFRESH_TIME) //velocidade
  {
      scan_velocidade = (3600*circMedida*contvolta)/(millis() - lastRefresh); 
    //velocidade = (3600*2*M_PI*raio*contvolta)/(millis() - lastRefresh);
      contvolta = 0;
      lastRefresh = millis();
  } 

}

//leitura do sensor de gas
void leitura_sensor_gas()
{
  ajuste_de_limites_mq7(sensor_gas_mq7); // funcao ajusta os valores maximos e minimos
  sensor_gas_mq7 = map(sensor_gas_mq7,menor_valor_lido, maior_valor_lido,0, 100);

}

// funcao para imprimir somente no LCD
void imprime_lcd()
{
  lcd.setCursor(0,0); //SET LINHA LCD
  lcd.print("IN: ");//printa LCD
  lcd.print(dht.readTemperature());
  lcd.print("C ");//printa LCD
  lcd.print(dht.readHumidity());
  lcd.print("%");//
  
  lcd.setCursor(0,1); //SET LINHA LCD
  lcd.print("Motor: ");//printa LCD
  lcd.print(ds18B20.getTempC(endereco_sensor_ds18B20));
  lcd.print(" C");  
    
  lcd.setCursor(0,2);
  lcd.print("CO: ");
  lcd.print(sensor_gas_mq7);
  lcd.print("% ");  
  
  lcd.print("Freio: "); //freio
  lcd.print(contador_freio); //freio
  lcd.setCursor(0,3);//REED
  lcd.print(scan_velocidade);//REED
  lcd.print(" km/h ");//REED
  
  lcd.print("Voltas: "); //VOLTAS
  lcd.print(contador_volta); //VOLTAS

}

