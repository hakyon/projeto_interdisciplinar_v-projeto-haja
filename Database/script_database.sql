--script para criação das tabelas  de no projeto Haja
--Autor :Alan Azevedo Bancks


-- DROP DATABASE dados_veiculo;

CREATE DATABASE dados_veiculo

-- DROP TABLE comandos;

CREATE TABLE public.comandos
(
    id_comando integer NOT NULL DEFAULT nextval('comandos_id_comando_seq'::regclass),
    comando character varying(255) COLLATE pg_catalog."default",
    data_envio timestamp without time zone,
    data_execucao timestamp without time zone
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.comandos
    OWNER to postgres;
	
	
-- DROP TABLE dados_sensores;
	
CREATE TABLE public.dados_sensores
(
    ds_id integer NOT NULL DEFAULT nextval('dados_sensores_ds_id_seq'::regclass),
    ds_velocidade bigint,
    ds_umidade bigint,
    ds_temperatura bigint,
    ds_monoxido bigint,
    ds_voltas bigint,
    ds_ocorrencia_freio bigint,
    ds_id_piloto bigint,
    ds_ultima_atualizacao timestamp without time zone,
    ds_status character varying(255) COLLATE pg_catalog."default",
    ds_estado_led_verde integer,
    ds_estado_led_amarelo integer,
    ds_estado_led_vermelho integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dados_sensores
    OWNER to postgres;



-- DROP TABLE piloto;

CREATE TABLE public.piloto
(
    id_piloto integer NOT NULL DEFAULT nextval('piloto_id_piloto_seq'::regclass),
    nome_piloto text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.piloto
    OWNER to postgres;	
	
