#ifndef CONTANTES_GLOBAIS_H
#define CONTANTES_GLOBAIS_H

#define LIMITE_VELOCIDADE 100
#define LIMITE_UMIDADE 70
#define TEMPERATURA 70
#define NIVEL_MONOXIDO_CARBONO 50

#define QTD_VALORES_LIDOS_VELOCIDADE 10
#define QTD_VALORES_LIDOS_UMIDADE    10
#define QTD_VALORES_LIDOS_TEMPERATURA 10
#define QTD_VALORES_LIDOS_MONOXIDO 10

#define NUMERO_DE_LEITURA_GERAL 20


#define UNIDADE_TEMPERATURA "°C"
#define UNIDADE_UMIDADE    "%"
#define UNIDADE_VELOCIDADE  "Km/h"
#define UNIDADE_MONOXIDO    "ppm"
#define UNIDADE_ACELERACAO  "m\\s²"


#define MAXIMO_TEMPERATURA_INTERNA 50
#define MINIMO_TEMPERATURA_INTERNA 0
#define MAXIMO_TEMPERATURA_EXTERNA 50
#define MINIMO_TEMPERATURA_EXTERNA 0
#define MAXIMO_UMIDADE 90
#define MINIMO_UMIDADE  20
#define MAXIMO_MONOXIDO 10000
#define MINIMO_MONOXIDO 10
#define MAXIMO_VELOCIDADE 60
#define MINIMO_VELOCIDADE 0
#define MINIMO_ACELERACAO 0
#define MAXIMO_ACELERACAO 15


//Banco de Dados PostgreSQL

#define BD_COMANDOS_HOST "localhost"
#define BD_COMANDOS_NAME "dados_veiculo"
#define BD_COMANDOS_PASSWORD "espada22"
#define BD_COMANDOS_PORT  5432
#define BD_COMANDOS_USER "postgres"


// sao posicoes das colunas de cada informacao
// id da linha seria posicao 0
#define POSICAO_VELOCIDADE_DB 1
#define POSICAO_UMIDADE_DB 2
#define POSICAO_TEMPERATURA_DB 3
#define POSICAO_MONOXIDO_DB 4
#define POSICAO_VOLTAS_DB 5
#define POSICAO_FREIADAS_DB 6
#define POSICAO_PILOTO_DB 7
#define POSICAO_DATA_ATUALIZADA_DB 8
#define POSICAO_STATUSVEICULO_DB 9
#define POSICAO_ESTADO_LED_VERDE_DB 10
#define POSICAO_ESTADO_LED_AMARELO_DB 11
#define POSICAO_ESTADO_LED_VERMELHO_DB 12




#endif // CONTANTES_GLOBAIS_H
