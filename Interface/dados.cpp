#include "dados.h"

/**
 * @brief Dados::Dados - essa classe carrega os atributos que serao consultados do veiculo , nada mais que acessores e mutantes de atributi
 */

Dados::Dados()
{
    comandos_status.clear();
    ultimas_leituras.clear();
    ultimas_leituras_graficos.clear();
}

QString Dados::getDataHora_UltimaAtualizacao() const
{
    return DataHora_UltimaAtualizacao;
}

void Dados::setDataHora_UltimaAtualizacao(const QString &value)
{
    DataHora_UltimaAtualizacao = value;
}

int Dados::getTemperatura() const
{
    return temperatura;
}

void Dados::setTemperatura(int value)
{
    temperatura = value;
}

QString Dados::getStatusDoVeiculo() const
{
    return statusDoVeiculo;
}

void Dados::setStatusDoVeiculo(const QString &value)
{
    statusDoVeiculo = value;
}

int Dados::getN_voltas() const
{
    return n_voltas;
}

void Dados::setN_voltas(int value)
{
    n_voltas = value;
}

int Dados::getUmidade() const
{
    return umidade;
}

void Dados::setUmidade(int value)
{
    umidade = value;
}

int Dados::getVelocidade() const
{
    return velocidade;
}

void Dados::setVelocidade(int value)
{
    velocidade = value;
}

int Dados::getNivelMonoxido() const
{
    return nivel_monoxido;
}

void Dados::setNivelMonoxido(int value)
{
    nivel_monoxido = value;
}

int Dados::getContagemFreios() const
{
    return contagem_freios;
}

void Dados::setContagemFreios(int value)
{
    contagem_freios += value;
}

int Dados::getLedVerde() const
{
    return estadoLedVerde;
}

void Dados::setLedVerde(int value)
{
    estadoLedVerde = value;
}

int Dados::getLedAmarelo() const
{
    return estadoLedAmarelo;
}

void Dados::setLedAmarelo(int value)
{
    estadoLedAmarelo = value;
}

int Dados::getLedVermelho() const
{
    return estadoLedVermelho;
}

void Dados::setLedVermelho(int value)
{
    estadoLedVermelho = value;
}
