#ifndef DADOS_H
#define DADOS_H

#include <QString>
#include <QStringList>
#include <QVector>

#include "contantes_globais.h"
class Dados
{
public:
    Dados();

    int velocidade = 0;
    int umidade = 0 ;
    int temperatura = 0;
    int n_voltas = 0;
    int nivel_monoxido = 0;
    int contagem_freios = 0;
    int estadoLedVerde = 0;
    int estadoLedAmarelo = 0;
    int estadoLedVermelho = 0;

    QString statusDoVeiculo;
    QString DataHora_UltimaAtualizacao;
    QVector<QStringList> comandos_status;
    QVector<QStringList> ultimas_leituras;
    QVector<QStringList> ultimas_leituras_graficos;


    int getVelocidade() const;
    void setVelocidade(int value);
    int getUmidade() const;
    void setUmidade(int value);
    int getN_voltas() const;
    void setN_voltas(int value);
    QString getStatusDoVeiculo() const;
    void setStatusDoVeiculo(const QString &value);
    int getTemperatura() const;
    void setTemperatura(int value);
    int getNivelMonoxido() const;
    void setNivelMonoxido(int value);
    int getContagemFreios() const;
    void setContagemFreios(int value);
    QString getDataHora_UltimaAtualizacao() const;
    void setDataHora_UltimaAtualizacao(const QString &value);
    int getLedVerde() const;
    void setLedVerde(int value);
    int getLedAmarelo() const;
    void setLedAmarelo(int value);
    int getLedVermelho() const;
    void setLedVermelho(int value);



};

#endif // DADOS_H
