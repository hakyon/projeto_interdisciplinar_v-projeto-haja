#include "databaseconexao.h"


/**
 * @brief DataBaseConexao::DataBaseConexao - responsavel por tratar da conexao com banco de dados
 * @param driver - TAG que diz o tipo de banco de dados que sera usado
 */
DataBaseConexao::DataBaseConexao(const char* driver,QString nome_conexao )
{
    if(nome_conexao.isEmpty()){
        db = new QSqlDatabase(QSqlDatabase::addDatabase(driver));
    }
    else{
        db = new QSqlDatabase(QSqlDatabase::addDatabase(driver,nome_conexao));
}

}

DataBaseConexao::~DataBaseConexao()
{
    qDebug() << "Destruindo a classe db";
    delete db;
}

QSqlDatabase *DataBaseConexao::connect(const QString &server,
                                       const QString &databaseName,
                                       const QString &userName,
                                       const QString &password,
                                       const int porta)
{
    db->setConnectOptions();
    db->setHostName(server);
    db->setDatabaseName(databaseName);
    db->setUserName(userName);
    db->setPassword(password);
    db->setPort(porta);



    if(db->open()){
        std::cout << "Conectado ao banco " <<endl;
        return db;
    }
    else{
        return NULL;
    }

}
/**Esse metodos abaixo a principio nao estao sendo usados,mas servem para abstrair a escrita do select para interagir ao banco  **/


//int DataBaseConexao::selectContagemDelinhas(QSqlQuery *query)
//{
//    bool queryRes = query->exec();
//       if (query->lastError().type() != QSqlError::NoError || !queryRes)
//       {
//           qDebug() << query->lastError().text();
//           return -1;
//       }

//       int recordCount = 0;
//       while (query->next())
//       {
//           qDebug() << "Campo 1 : " << query->value(0).toString()
//                    << "Campo 2 : " << query->value(1).toString();
//           ++recordCount;
//       }

//       return recordCount;
//}

bool DataBaseConexao::executarInsert(QSqlQuery *query)
{
    db->transaction();
       bool queryRes = query->exec();
       if (query->lastError().type() != QSqlError::NoError || !queryRes)
       {
           qDebug() << query->lastError().text();
           db->rollback();
           return false;
       }
       db->commit();
       return true;
}

//bool DataBaseConexao::executarUpdate(QSqlQuery *query)
//{
//    db->transaction();
//    bool queryRes = query->exec();
//    if (query->lastError().type() != QSqlError::NoError || !queryRes)
//    {
//        qDebug() << query->lastError().text();
//        db->rollback();
//        return false;
//    }
//    db->commit();
//    return true;
//}

//bool DataBaseConexao::executarDelete(QSqlQuery *query)
//{
//    db->transaction();
//    bool queryRes = query->exec();
//    if (query->lastError().type() != QSqlError::NoError || !queryRes)
//    {
//        qDebug() << query->lastError().text();
//        db->rollback();
//        return false;
//    }
//    db->commit();
//    return true;
//}

void DataBaseConexao::disconnect()
{
    qDebug() << "Desconectado do banco de dados!";
    db->close();
    QSqlDatabase::removeDatabase("qt_sql_default_connection");

}

