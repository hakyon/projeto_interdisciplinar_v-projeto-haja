#ifndef DATABASECONEXAO_H
#define DATABASECONEXAO_H

#include <QtSql>
#include <QString>
#include <QDebug>

#include <iostream>

class DataBaseConexao
{
public:
    DataBaseConexao(const char* driver,QString nome_conexao);
    ~DataBaseConexao();
    QSqlDatabase * connect(const QString& server,
                              const QString& databaseName,
                              const QString& userName,
                              const QString& password,
                              const int porta);
    void disconnect();
    int selectContagemDelinhas(QSqlQuery * query);
    bool executarInsert(QSqlQuery * query);
    bool executarUpdate(QSqlQuery * query);
    bool executarDelete(QSqlQuery * query);




private:
    QSqlDatabase *db;
};

#endif // DATABASECONEXAO_H
