/*******************************************************************/
/*Autor: Alan Azevedo Bancks                                       */
/*                                                                 */
/*Projeto:Interface Desktop para recepção de dados de Veiculo      */
/*Data:                                                            */
/*******************************************************************/

#include "mainwindow.h"
#include <QApplication>
#include "databaseconexao.h"
#include "dados.h"
#include "persistencia.h"
#include "contantes_globais.h"

#include <QSysInfo>

/**
 * @brief conectarAoBanco - responsavel por executar interacao no banco de dados
 * @param dados -  instancia do objeto que carrega as informacoes do veiculo
 * @param db -  instancia da configuracao de acesso ao banco de dados
 */

void conectarAoBanco(Dados *dados , QSqlDatabase* db)
{

    QSqlQuery query("SELECT * from dados_sensores ORDER BY ds_id DESC LIMIT 1");
    while (query.next()) {
        dados->setVelocidade(query.value(POSICAO_VELOCIDADE_DB).toInt());
        dados->setUmidade(query.value(POSICAO_UMIDADE_DB).toInt());
        dados->setTemperatura(query.value(POSICAO_TEMPERATURA_DB).toInt());
        dados->setN_voltas(query.value(POSICAO_VOLTAS_DB).toInt());
        dados->setStatusDoVeiculo(query.value(POSICAO_STATUSVEICULO_DB).toString());
        dados->setNivelMonoxido(query.value(POSICAO_MONOXIDO_DB).toInt());
        dados->setContagemFreios(query.value(POSICAO_FREIADAS_DB).toInt());
        dados->setDataHora_UltimaAtualizacao(query.value(POSICAO_DATA_ATUALIZADA_DB).toDateTime().toString("dd/MM/yyyy HH:mm:ss"));
        dados->setLedVerde(query.value(POSICAO_ESTADO_LED_VERDE_DB).toInt());
        dados->setLedAmarelo(query.value(POSICAO_ESTADO_LED_AMARELO_DB).toInt());
        dados->setLedVermelho(query.value(POSICAO_ESTADO_LED_VERMELHO_DB).toInt());
    }


}

/**
 * @brief carregarUltimosComandos- busca no banco de dados os ultimos comandos enviado na primeira chamada da tabela pelo main
 * @param dados -  instancia do objeto que carrega as informacoes do veiculo
 * @param db -  instancia da configuracao de acesso ao banco de dados
 */
void carregarUltimosComandos(Dados *dados ,QSqlDatabase* db)
{

    QSqlQuery query("SELECT * FROM (SELECT * FROM comandos ORDER BY id_comando DESC LIMIT 20 ) AS SUB ORDER BY id_comando ASC");

    while (query.next()) {
        QStringList tmp;
        QSqlRecord record = query.record();
        for(int i=0; i < record.count() +1; i++){

            if(i == 2)
            {
                tmp.append(record.value(2).toDateTime().toString("dd/MM/yyyy HH:mm:ss"));
                continue;
            }
            if(i == 3)
            {
                if(record.value(3).isNull())
                    tmp.append("Não Executado");
                else
                    tmp.append(record.value(3).toDateTime().toString("dd/MM/yyyy HH:mm:ss"));

                continue;
            }
            else
                tmp.append(record.value(i).toString());
        }
        dados->comandos_status.append(tmp);
    }

}

/**
 * @brief carregarUltimasLeituras - busca ultimas leituras dos sensores e carrega em uma lista para ser usado depois nos graficos
 * @param dados -  instancia do objeto que carrega as informacoes do veiculo
 * @param db -  instancia da configuracao de acesso ao banco de dados
 */
void carregarUltimasLeituras(Dados *dados ,QSqlDatabase* db)
{
    QSqlQuery query("SELECT * from dados_sensores ORDER BY ds_id DESC LIMIT 20");

    while (query.next()) {
        QStringList tmp;
        QSqlRecord record = query.record();
        for(int i=0; i < record.count(); i++){
            tmp.append(record.value(i).toString());

            if(i==6) // quando pega um tipo data
            {
                tmp.append(record.value(POSICAO_DATA_ATUALIZADA_DB).toDateTime().toString("dd/MM/yyyy HH:mm:ss:zzz"));
            }
        }
        dados->ultimas_leituras.append(tmp);
    }


    QSqlQuery query2("SELECT * FROM (SELECT * from dados_sensores ORDER BY ds_id DESC LIMIT 20) AS SUB ORDER BY ds_id ASC");

    //esse valores sao somente para o grafico
    while(query2.next())
    {
        QStringList tmp;
        QSqlRecord record = query2.record();
        for(int i=0; i < record.count(); i++){

            tmp.append(record.value(i).toString());
            if(i==6){
                tmp.append(record.value(POSICAO_DATA_ATUALIZADA_DB).toDateTime().toString("HH:mm:ss:zzz")); // vem ordenado diferente
            }
        }
        dados->ultimas_leituras_graficos.append(tmp);
    }


}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //informacoes de compilação
    qDebug() << "Compilado com versao de Qt = " << QT_VERSION_STR;
    qDebug() << "Compilado em S.O = " << QSysInfo::prettyProductName();
    qDebug() << "Arquitetura de build = " <<  QSysInfo::buildCpuArchitecture();
    qDebug() << "Arquiteura da CPU de compilação = " << QSysInfo::currentCpuArchitecture();

    QString conexao;
    //conexao = "Recebimento_Dados";


    const char* driverName = "QPSQL";
    DataBaseConexao* dadosconexao = new DataBaseConexao(driverName,conexao);
    QSqlDatabase* db = dadosconexao->connect(BD_COMANDOS_HOST,BD_COMANDOS_NAME,BD_COMANDOS_USER,BD_COMANDOS_PASSWORD,BD_COMANDOS_PORT);

    Dados *dados = new Dados();
    conectarAoBanco(dados,db);
    carregarUltimosComandos(dados,db);
    carregarUltimasLeituras(dados,db);

//    QThread* thread = new QThread();
//    Persistencia* worker = new Persistencia(3000);
//    worker->moveToThread(thread);
//    QObject::connect(thread, SIGNAL(started()), worker, SLOT(start()));

//    thread->start();

    //fluxo de teste sem banco
    MainWindow *dadosAtuais;
    dadosAtuais = new MainWindow(*dados);
    dadosAtuais->setWindowTitle("Diagnostico - Veiculo (V.1.62)");
    dadosAtuais->setFixedSize(1300,900);
    dadosAtuais->show();
    dadosAtuais->ExibirGraficoDeVelocidade();
    dadosAtuais->ExibirGraficoDeTemperatura();
    dadosAtuais->ExibirGraficoDeUmidade();
    dadosAtuais->ExibirGraficoDeMonoxidoCarbono();
    //

    return a.exec();

}

