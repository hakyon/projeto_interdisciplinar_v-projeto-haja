#include "mainwindow.h"
#include "persistencia.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <stdint.h>
#include <thread>
#include <QPixmap>
/**
 * @brief MainWindow::MainWindow - construtor dessa classe captura os valores de Dodos afim de inicializar a tela
 * @param dadosTela - instancia do objeto dados
 * @param parent - instancia que herda da classe Widget
 */
MainWindow::MainWindow(Dados &dadosTela, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{



    ui->setupUi(this);
    ui->tableComados->setRowCount(NUMERO_DE_LEITURA_GERAL);
    ui->tableComados->setColumnCount(4);
    ui->tableComados->setHorizontalHeaderItem(0, new QTableWidgetItem("Id"));
    ui->tableComados->setHorizontalHeaderItem(1, new QTableWidgetItem("Comando"));
    ui->tableComados->setHorizontalHeaderItem(2, new QTableWidgetItem("Data de Envio"));
    ui->tableComados->setHorizontalHeaderItem(3, new QTableWidgetItem("Data de Execução"));

    ui->tableComados->setColumnWidth(0,30);
    ui->tableComados->setColumnWidth(1,150);
    ui->tableComados->setColumnWidth(2,200);
    ui->tableComados->setColumnWidth(3,200);

    for(int i=0; i< ui->tableComados->rowCount(); i++)
    {
        for(int j=0; j< ui->tableComados->columnCount(); j++)
        {
            QTableWidgetItem *pCell =  ui->tableComados->item(i, j);
            if(!pCell)
            {
                pCell = new QTableWidgetItem;
                ui->tableComados->setItem(i, j, pCell);
            }
            if(dadosTela.comandos_status.at(i).at(j) == NULL){
                goto sairloops;
            }
            else
               pCell->setText(dadosTela.comandos_status.at(i).at(j));
        }
    }

    sairloops:
    this->ultimas_leituras_sensores = dadosTela.ultimas_leituras_graficos;
    this->ultimos_comandos = dadosTela.comandos_status;

    ui->lcdVelocidadeDisplay->display(dadosTela.getVelocidade());
    ui->lcdMonoxidoDisplay->display(dadosTela.getNivelMonoxido());
    ui->lcdUmidadeDisplay->display(dadosTela.getUmidade());
    ui->lcdTemperaturaMotorDisplay->display(dadosTela.getTemperatura());
    ui->lcdDisplayNVoltas->display(dadosTela.getN_voltas());
    ui->label_dataUltimaAtualizacao->setText(dadosTela.getDataHora_UltimaAtualizacao());


    ui->labelStatusVeiculo->setText( dadosTela.getStatusDoVeiculo());
    ui->labelQualidadeSinal->setText("Sem Informação");
    CalcularAceleracao();

    QString format_a;
    ui->lcdAceleracaoMediaDisplay->display(format_a.sprintf("%+06.3f", this->aceleracaoMedia));

    this->velocidadeMomento = dadosTela.getVelocidade();
    this->temperaturaMomento = dadosTela.getTemperatura();
    this->umidadeMomento = dadosTela.getUmidade();
    this->statusMomento = dadosTela.getStatusDoVeiculo();
    this->n_voltas = dadosTela.getN_voltas();
    this->nivel_monoxido = dadosTela.getNivelMonoxido();
    this->contagem_freios = dadosTela.getContagemFreios();

    ui->toolButton->setText("Sobre");


    ui->labelVelocidade->setText(QString ("Velocidade (%1)").arg(UNIDADE_VELOCIDADE));
    ui->labelUmidade->setText(QString ("Umidade (%1)").arg(UNIDADE_UMIDADE));
    ui->labelAceleracao->setText(QString ("Aceleração ~ (%1)").arg(UNIDADE_ACELERACAO));
    ui->labelTemperatura->setText(QString ("Temperatura (%1)").arg(UNIDADE_TEMPERATURA));
    ui->labelTemperaturaMotor->setText(QString ("Temp. Motor (%1)").arg(UNIDADE_TEMPERATURA));
    ui->labelMonoxido->setText(QString ("CO (%1)").arg(UNIDADE_MONOXIDO));


    pColor.setColor(QPalette::Highlight, Qt::green);
    pColor2.setColor(QPalette::Highlight, Qt::yellow);
    pColor3.setColor(QPalette::Highlight, Qt::red);

    //coisas nao implementadas ainda
    ui->BtnEnviaMenssagem->setEnabled(false);
    ui->lineEditCustomMessage->setEnabled(false);

    StatusVelocidade();
    StatusAceleracao();
    StatusUmidade();
    StatusMonoxido();
    StatusTemperaturaMotor();


    //Nao implementado
    ui->limiteTemperaturaMotor->setValue(0);
    VerificarEstadosLeds();

}

MainWindow::~MainWindow()
{
    delete ui;
}
/**
 * @brief MainWindow::StatusVelocidade - responsavel setar velocidade na tela e tratar seu limite
 */
void MainWindow::StatusVelocidade()
{

    int velocidade = this->velocidadeMomento * 100 / MAXIMO_VELOCIDADE;

    if(velocidade > 100){
        ui->limiteVelocidade->setValue(100);
        ui->limiteVelocidade->setPalette(pColor3);
    }
    else if(velocidade > 80)
    {
        ui->limiteVelocidade->setValue(velocidade);
        ui->limiteVelocidade->setPalette(pColor3);
    }
    else if(velocidade > 50 && velocidade < 80)
    {
        ui->limiteVelocidade->setValue(velocidade);
        ui->limiteVelocidade->setPalette(pColor2);
    }
    else
    {
        ui->limiteVelocidade->setValue(velocidade);
        ui->limiteVelocidade->setPalette(pColor);
    }



}

/**
 * @brief MainWindow::StatusMonoxido - responsavel setar nivel de monoxido na tela e tratar seu limite
 */
void MainWindow::StatusMonoxido()
{

    int co = this->nivel_monoxido * 100 / MAXIMO_MONOXIDO;

    if(co > 100){
        ui->limiteCO->setValue(100);
        ui->limiteCO->setPalette(pColor3);
    }
    else if(co > 80)
    {
        ui->limiteCO->setValue(co);
        ui->limiteCO->setPalette(pColor3);
    }
    else if(co > 50 && co < 80)
    {
        ui->limiteCO->setValue(co);
        ui->limiteCO->setPalette(pColor2);
    }
    else
    {
        ui->limiteCO->setValue(co);
        ui->limiteCO->setPalette(pColor);
    }


}

void MainWindow::StatusAceleracao()
{

    ui->limiteAceleracao->setValue((this->aceleracaoMedia * 100 / MAXIMO_ACELERACAO));

    if((this->aceleracaoMedia * 100 / MAXIMO_ACELERACAO) > 80)
    {
        ui->limiteAceleracao->setPalette(pColor3);
    }
    else if( (this->aceleracaoMedia * 100 / MAXIMO_ACELERACAO) > 50 &&
             (this->aceleracaoMedia * 100 / MAXIMO_ACELERACAO) < 80)
    {
        ui->limiteAceleracao->setPalette(pColor2);
    }


}

/**
 * @brief MainWindow::StatusUmidade - responsavel setar a umidade na tela e tratar seu limite
 */

void MainWindow::StatusUmidade()
{
    ui->limiteUmidade->setValue((this->umidadeMomento * 100 / this->MaxUmidade));

    if((this->umidadeMomento * 100 / this->MaxUmidade) > 80)
    {
        ui->limiteUmidade->setPalette(pColor);
    }
    else if( (this->umidadeMomento * 100 / this->MaxUmidade) > 50 &&
             (this->umidadeMomento * 100 / this->MaxUmidade) < 80)
    {
        ui->limiteUmidade->setPalette(pColor2);
    }

}

/**
 * @brief MainWindow::StatusTemperaturaMotor - responsavel inserir a temperatura na tela e tratar os valores do limite fisico
 */
void MainWindow::StatusTemperaturaMotor()
{
    int temperatura = this->temperaturaMomento * 100 / MAXIMO_TEMPERATURA_EXTERNA;

    if(temperatura > 100){
        ui->limiteTemperatura->setValue(100);
        ui->limiteTemperatura->setPalette(pColor3);
    }
    else if(temperatura > 80)
    {
        ui->limiteTemperatura->setValue(temperatura);
        ui->limiteTemperatura->setPalette(pColor3);
    }
    else if(temperatura > 50 && temperatura < 80)
    {
        ui->limiteTemperatura->setValue(temperatura);
        ui->limiteTemperatura->setPalette(pColor2);
    }
    else
    {
        ui->limiteTemperatura->setValue(temperatura);
        ui->limiteTemperatura->setPalette(pColor);
    }
}
/**
 * @brief MainWindow::StatusQualidadeSinal  - vai buscar apartir do servidor e informar qualidade do sinal de transmisao
 */
void MainWindow::StatusQualidadeSinal()
{

}
/**
 * @brief MainWindow::StatusVeiculo
 */
void MainWindow::StatusVeiculo()
{

}

/**
 * @brief MainWindow::AtualizandoDados - e chamada todo momento pela Thread para atualizar os dados a cada segundo
 * @param dadosAtualizados -  sao novos dados carregados da consulta do banco
 */
void MainWindow::AtualizandoDados(Dados *dadosAtualizados)
{
    this->velocidadeMomento = dadosAtualizados->velocidade;
    this->temperaturaMomento = dadosAtualizados->temperatura;
    this->umidadeMomento = dadosAtualizados->umidade;
    this->statusMomento = dadosAtualizados->getStatusDoVeiculo();
    this->nivel_monoxido = dadosAtualizados->getNivelMonoxido();
    this->contagem_freios = dadosAtualizados->getContagemFreios();
    ui->lcdVelocidadeDisplay->display(this->velocidadeMomento);
    ui->lcdUmidadeDisplay->display(this->umidadeMomento);
    ui->lcdTemperaturaMotorDisplay->display(this->temperaturaMomento);
    ui->lcdDisplayNVoltas->display(dadosAtualizados->getN_voltas());
    ui->label_dataUltimaAtualizacao->setText(dadosAtualizados->getDataHora_UltimaAtualizacao());


    ui->labelStatusVeiculo->setText( dadosAtualizados->getStatusDoVeiculo());



    this->StatusUmidade();
    this->StatusVelocidade();
    this->StatusAceleracao();
    this->StatusTemperaturaMotor();
    this->StatusVeiculo();
    this->StatusMonoxido();

    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    //this->ExibirGraficoDeVelocidade();

    cout << "Dados atualizados :" << asctime (timeinfo) << endl;

}

/**
 * @brief MainWindow::ExibirGraficoDeVelocidade - método responsavel por carregar grafico da velocidade
 *
 */

void MainWindow::ExibirGraficoDeVelocidade()
{
    seriesVelocidade = new QLineSeries();


    QStringList valor_grafico;
    QStringList valor_graficoTempo;


    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_VELOCIDADE_DB)); //velocidade
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }


    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesVelocidade->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }

    //*series << QPointF(13,4) << QPointF(14,5)  ; // primeiro valor eh o ponto mais alto no comprimento, segundo valor eh o valor mais alto na altura
    //ponto mais alto em x, ponto mais alto em Y


    chartVelocidade = new QChart();
    chartVelocidade->legend()->hide();
    chartVelocidade->addSeries(seriesVelocidade);
    chartVelocidade->createDefaultAxes();
    chartVelocidade->setTitle("Velocidade/Tempo");

    axisXVelocidade = new QDateTimeAxis;
    axisXVelocidade->setFormat("HH:mm:ss");
    axisXVelocidade->setTitleText("Período");

    axisYVelocidade = new QDateTimeAxis;
    axisYVelocidade->setTitleText("Km/h");


    chartViewVelocidade = new QChartView(chartVelocidade);
    chartViewVelocidade->setRenderHint(QPainter::Antialiasing);

    chartViewVelocidade->chart()->setAxisX(axisXVelocidade,seriesVelocidade);


    viewWidgetVelocidade = new QMainWindow();
    viewWidgetVelocidade->setCentralWidget(chartViewVelocidade);


    ui->tabGrafico->removeTab(0);
    ui->tabGrafico->removeTab(0);

    ui->tabGrafico->addTab(viewWidgetVelocidade,"Velocidade");
    ui->tabVelocidade->setVisible(false);
    ui->tabTemperatura->setVisible(false);



}

/**
 * @brief MainWindow::ExibirGraficoDeVelocidade - método responsavel por carregar grafico da temperatura
 *
 */
void MainWindow::ExibirGraficoDeTemperatura()
{
    seriesTemperatura = new QLineSeries();

    QStringList valor_grafico;
    QStringList valor_graficoTempo;


    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_TEMPERATURA_DB)); // temperatura
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }


    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesTemperatura->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartTemperatura = new QChart();
    chartTemperatura->legend()->hide();
    chartTemperatura->addSeries(seriesTemperatura);
    chartTemperatura->createDefaultAxes();
    chartTemperatura->setTitle("Temperatura/tempo");

    axisXTemperatura = new QDateTimeAxis;
    axisXTemperatura->setFormat("HH:mm:ss");
    axisXTemperatura->setTitleText("Período");

    axisYTemperatura = new QDateTimeAxis;
    axisYTemperatura->setTitleText(UNIDADE_TEMPERATURA);

    chartViewTemperatura = new QChartView(chartTemperatura);
    chartViewTemperatura->setRenderHint(QPainter::Antialiasing);

    chartViewTemperatura->chart()->setAxisX(axisXTemperatura,seriesTemperatura);

    viewWidgetTemperatura = new QMainWindow();
    viewWidgetTemperatura->setCentralWidget(chartViewTemperatura);
    ui->tabGrafico->addTab(viewWidgetTemperatura,"Temperatura");

    ui->tabVelocidade->setVisible(false);
    ui->tabTemperatura->setVisible(false);


}


/**
 * @brief MainWindow::ExibirGraficoDeVelocidade - método responsavel por carregar grafico da Humidade
 *
 */
void MainWindow::ExibirGraficoDeUmidade()
{
    seriesUmidade = new QLineSeries();

    QStringList valor_grafico;
    QStringList valor_graficoTempo;


    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_UMIDADE_DB)); // humidade
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }


    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesUmidade->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartUmidade = new QChart();
    chartUmidade->legend()->hide();
    chartUmidade->addSeries(seriesUmidade);
    chartUmidade->createDefaultAxes();
    chartUmidade->setTitle("Umidade/tempo");

    axisXUmidade = new QDateTimeAxis;
    axisXUmidade->setFormat("HH:mm:ss");
    axisXUmidade->setTitleText("Período");

    axisYUmidade = new QDateTimeAxis;
    axisYUmidade->setTitleText(UNIDADE_UMIDADE);

    chartViewUmidade = new QChartView(chartUmidade);
    chartViewUmidade->setRenderHint(QPainter::Antialiasing);

    chartViewUmidade->chart()->setAxisX(axisXUmidade,seriesUmidade);

    viewWidgetUmidade = new QMainWindow();
    viewWidgetUmidade->setCentralWidget(chartViewUmidade);
    ui->tabGrafico->addTab(viewWidgetUmidade,"Umidade");

    ui->tabVelocidade->setVisible(false);
    ui->tabTemperatura->setVisible(false);


}

/**
 * @brief MainWindow::ExibirGraficoDeVelocidade - método responsavel por carregar grafico dos niveis de Monóxido de Carbono
 *
 */
void MainWindow::ExibirGraficoDeMonoxidoCarbono()
{
    seriesMonoxido = new QLineSeries();

    QStringList valor_grafico;
    QStringList valor_graficoTempo;


    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_MONOXIDO_DB)); // monoxido
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }


    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesMonoxido->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartMonoxido = new QChart();
    chartMonoxido->legend()->hide();
    chartMonoxido->addSeries(seriesMonoxido);
    chartMonoxido->createDefaultAxes();
    chartMonoxido->setTitle("Umidade/tempo");

    axisXMonoxido = new QDateTimeAxis;
    axisXMonoxido->setFormat("HH:mm:ss");
    axisXMonoxido->setTitleText("Período");

    axisYMonoxido = new QDateTimeAxis;
    axisYMonoxido->setTitleText(UNIDADE_MONOXIDO);

    chartViewMonoxido = new QChartView(chartMonoxido);
    chartViewMonoxido->setRenderHint(QPainter::Antialiasing);

    chartViewMonoxido->chart()->setAxisX(axisXMonoxido,seriesMonoxido);

    viewWidgetMonoxido = new QMainWindow();
    viewWidgetMonoxido->setCentralWidget(chartViewMonoxido);
    ui->tabGrafico->addTab(viewWidgetMonoxido,"Monoxido de Carbono");



}





/**
 * @brief MainWindow::on_BtnLigaVeiculo_clicked - método acionado por botão responsavel por enviar comando para ligar veiculo
 *
 */
void MainWindow::on_BtnLigaVeiculo_clicked()
{

    QString cmd = "LIGA_VEICULO";
    EnviaComandos(cmd);
}

/**
 * @brief MainWindow::on_BtnLigaVeiculo_clicked - método acionado por botão responsavel por enviar comando para desligar veiculo
 *
 */
void MainWindow::on_BtnDesligarCarro_clicked()
{

    QString cmd = "DESLIGA_VEICULO";
    EnviaComandos(cmd);
}
/**
 * @brief MainWindow::on_BtnLigaVeiculo_clicked - método acionado por botão responsavel por enviar comando para sinal amarelo
 *
 */
void MainWindow::on_BtnSinalAmarelo_clicked()
{

    QString cmd = "SINAL_AMARELO";
    EnviaComandos(cmd);
}

/**
 * @brief MainWindow::on_BtnLigaVeiculo_clicked - método acionado por botão responsavel por enviar comando para sinal verde
 *
 */
void MainWindow::on_BtnSinalVerde_clicked()
{

    QString cmd = "SINAL_VERDE";
    EnviaComandos(cmd);
}
/**
 * @brief MainWindow::on_BtnLigaVeiculo_clicked - método acionado por botão responsavel por enviar comando para sinal vermelho
 *
 */

void MainWindow::on_btnSinalVermelho_clicked()
{

    QString cmd = "SINAL_VERMELHO";
    EnviaComandos(cmd);
}

/**
 * @brief MainWindow::on_BtnLigaVeiculo_clicked - método acionado por botão responsavel por enviar comando para enviar menssagem personalizada
 *
 */
void MainWindow::on_BtnEnviaMenssagem_clicked() //nao implementado ainda
{
    QString cmd;
    QString msg = ui->lineEditCustomMessage->text();
    cmd = "MSG BOXE : " + msg;
    EnviaComandos(cmd);
}

/**
 * @brief MainWindow::EnviaComandos - Método responsavel por enviar todos os comandos por interacao de tela para a camada de serviço
 * @param cmd -  comando que sera enviado para base de dados e consultado pelo serviço
 */
void MainWindow::EnviaComandos(QString cmd)
{
    const char* conexao = "Envio_Dados";
    QString data_agora = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");

    const char* driverName = "QPSQL";
    DataBaseConexao* conexaodb = new DataBaseConexao(driverName,conexao);
    QSqlDatabase* db = conexaodb->connect(BD_COMANDOS_HOST,BD_COMANDOS_NAME,BD_COMANDOS_USER,BD_COMANDOS_PASSWORD,BD_COMANDOS_PORT);

    QString query;
    if(db->isOpen()){
        QSqlQuery query_ex;
        query_ex.prepare("INSERT INTO comandos (comando,data_envio) values(:comando,:data_envio)");
        query_ex.bindValue(":comando",cmd);
        query_ex.bindValue(":data_envio",data_agora);
        qDebug()<<query_ex.exec()<<endl;
    }
    db->close();

}


/**
 * @brief on_btnAtualizarUltimosComandos_clicked - Método responsavel por atualizar a tabela com status dos ultimos comandos enviados
 * @param
 */
void MainWindow::on_btnAtualizarUltimosComandos_clicked()
{
    const char* conexao = "Status_comandos";

    const char* driverName = "QPSQL";
    DataBaseConexao* conexaodb = new DataBaseConexao(driverName,conexao);
    conexaodb->connect(BD_COMANDOS_HOST,BD_COMANDOS_NAME,BD_COMANDOS_USER,BD_COMANDOS_PASSWORD,BD_COMANDOS_PORT);

    QSqlQuery query("SELECT * FROM (SELECT * FROM comandos ORDER BY id_comando DESC LIMIT 20 ) AS SUB ORDER BY id_comando ASC");


    ultimas_leituras_sensores.clear();
    while (query.next()) {
        QStringList tmp;
        QSqlRecord record = query.record();
        for(int i=0; i < record.count(); i++){

            if(i == 2)
            {
                tmp.append(record.value(2).toDateTime().toString("dd'/'MM'/'yyyy HH:mm:ss"));
                continue;
            }
            if(i == 3)
            {
                if(record.value(3).isNull())
                    tmp.append("Não Executado");
                else
                    tmp.append(record.value(3).toDateTime().toString("dd/MM/yyyy HH:mm:ss"));

                continue;
            }
            tmp.append(record.value(i).toString());
        }
        ultimas_leituras_sensores.append(tmp);
    }

    ui->tableComados->clear();

    ui->tableComados->setHorizontalHeaderItem(0, new QTableWidgetItem("Id"));
    ui->tableComados->setHorizontalHeaderItem(1, new QTableWidgetItem("Comando"));
    ui->tableComados->setHorizontalHeaderItem(2, new QTableWidgetItem("Data de Envio"));
    ui->tableComados->setHorizontalHeaderItem(3, new QTableWidgetItem("Data de Execução"));

    for(int i=0; i< ui->tableComados->rowCount(); i++)
    {
        for(int j=0; j< ui->tableComados->columnCount(); j++)
        {
            QTableWidgetItem *pCell =  ui->tableComados->item(i, j);
            if(!pCell)
            {
                pCell = new QTableWidgetItem;
                ui->tableComados->setItem(i, j, pCell);
            }
            if(ultimas_leituras_sensores.at(i).at(j) == NULL){
                goto sairloops;
            }
            else
               pCell->setText(ultimas_leituras_sensores.at(i).at(j));
        }
    }
    sairloops:
    AtualizarValoresGraficos();

    AtualizarLeiturasDeComandos();

    AtualizarValoresGraficos();
    AtualizarGraficosVelocidade();
    AtualizarGraficosTemperatura();
    AtualizarGraficosMonoxido();
    AtualizarGraficosUmidade();
    VerificarEstadosLeds();


    AtualizarDisplay();
}

void MainWindow::AtualizarValoresGraficos()
{
    ultimas_leituras_sensores.clear();
    QSqlQuery query("SELECT * FROM (SELECT * from dados_sensores ORDER BY ds_id DESC LIMIT 20) AS SUB ORDER BY ds_id ASC");

    //esse valores sao somente para o grafico
    while(query.next())
    {
        QStringList tmp;
        QSqlRecord record = query.record();
        for(int i=0; i < record.count(); i++){

            tmp.append(record.value(i).toString());
            if(i==6){
                tmp.append(record.value(POSICAO_DATA_ATUALIZADA_DB).toDateTime().toString("HH:mm:ss:zzz")); // vem ordenado diferente
            }
        }
        this->ultimas_leituras_sensores.append(tmp);

    }

}

void MainWindow::AtualizarGraficosVelocidade()
{

    QStringList valor_grafico;
    QStringList valor_graficoTempo;
    seriesVelocidade = new QLineSeries();

    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_VELOCIDADE_DB)); //velocidade
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }

    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesVelocidade->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartVelocidade->addSeries(seriesVelocidade);
    chartVelocidade->createDefaultAxes();
    chartVelocidade->setTitle("Velocidade/Tempo");

    axisYVelocidade = new QDateTimeAxis;
    axisYVelocidade->setTitleText(UNIDADE_VELOCIDADE);

    chartViewVelocidade = new QChartView(chartVelocidade);
    chartViewVelocidade->setRenderHint(QPainter::Antialiasing);


    axisXVelocidade = new QDateTimeAxis;
    axisXVelocidade->setTitleText("Período");
    axisXVelocidade->setFormat("HH:mm:ss");
    chartViewVelocidade->chart()->setAxisX(axisXVelocidade,seriesVelocidade);

    viewWidgetVelocidade->setCentralWidget(chartViewVelocidade);

}



void MainWindow::AtualizarGraficosTemperatura()
{

    QStringList valor_grafico;
    QStringList valor_graficoTempo;
    seriesTemperatura = new QLineSeries();

    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_TEMPERATURA_DB)); //velocidade
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }

    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesTemperatura->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartTemperatura->addSeries(seriesTemperatura);
    chartTemperatura->createDefaultAxes();
    chartTemperatura->setTitle("Temperatura/Tempo");

    axisYTemperatura = new QDateTimeAxis;
    axisYTemperatura->setTitleText(UNIDADE_TEMPERATURA);

    chartViewTemperatura = new QChartView(chartTemperatura);
    chartViewTemperatura->setRenderHint(QPainter::Antialiasing);


    axisXTemperatura= new QDateTimeAxis;
    axisXTemperatura->setTitleText("Período");
    axisXTemperatura->setFormat("HH:mm:ss");
    chartViewTemperatura->chart()->setAxisX(axisXTemperatura,seriesTemperatura);

    viewWidgetTemperatura->setCentralWidget(chartViewTemperatura);

}


void MainWindow::AtualizarGraficosUmidade()
{

    QStringList valor_grafico;
    QStringList valor_graficoTempo;
    seriesUmidade = new QLineSeries();

    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_UMIDADE_DB)); //velocidade
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }

    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesUmidade->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartUmidade->addSeries(seriesUmidade);
    chartUmidade->createDefaultAxes();
    chartUmidade->setTitle("Umidade/Tempo");

    axisYUmidade = new QDateTimeAxis;
    axisYUmidade->setTitleText(UNIDADE_UMIDADE);

    chartViewUmidade = new QChartView(chartUmidade);
    chartViewUmidade->setRenderHint(QPainter::Antialiasing);


    axisXUmidade = new QDateTimeAxis;
    axisXUmidade->setTitleText("Período");
    axisXUmidade->setFormat("HH:mm:ss");
    chartViewUmidade->chart()->setAxisX(axisXUmidade,seriesUmidade);

    viewWidgetUmidade->setCentralWidget(chartViewUmidade);

}


void MainWindow::AtualizarGraficosMonoxido()
{

    QStringList valor_grafico;
    QStringList valor_graficoTempo;
    seriesMonoxido = new QLineSeries();

    for(int i=0;i<this->ultimas_leituras_sensores.size();i++)
    {

        valor_grafico.append(this->ultimas_leituras_sensores.at(i).at(POSICAO_MONOXIDO_DB)); //monoxido
        valor_graficoTempo.append(this->ultimas_leituras_sensores.at(i).at(7)); // tempo

    }

    for(int i=0;i<=valor_grafico.size()-1;i++)
    {
         QString tempo;
         QString valorCorrente;
         valorCorrente = valor_grafico.at(i);

         tempo = valor_graficoTempo.at(i);
         QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");
         seriesMonoxido->append(momento.toMSecsSinceEpoch(), valorCorrente.toInt());

    }


    chartMonoxido->addSeries(seriesMonoxido);
    chartMonoxido->createDefaultAxes();
    chartMonoxido->setTitle("CO/Tempo");

    axisYMonoxido = new QDateTimeAxis;
    axisYMonoxido->setTitleText(UNIDADE_MONOXIDO);

    chartViewMonoxido = new QChartView(chartMonoxido);
    chartViewMonoxido->setRenderHint(QPainter::Antialiasing);


    axisXMonoxido = new QDateTimeAxis;
    axisXMonoxido->setTitleText("Período");
    axisXMonoxido->setFormat("HH:mm:ss");
    chartViewMonoxido->chart()->setAxisX(axisXMonoxido,seriesMonoxido);

    viewWidgetMonoxido->setCentralWidget(chartViewMonoxido);

}



/**
 * @brief CalcularAceleracao - Método responsavel por calcular aceleração média apartir de um intervalo
 * de leitura equivalente a exibição do grafico
 * @param
 */
void MainWindow::CalcularAceleracao()
{
    int tempo_inicial = 0;
    int tempo_final = 0;
    int velocidade_inicial = 0;
    int velocidade_final = 0;

    for(int i=0;i<=this->ultimas_leituras_sensores.size();i++)
    {
        if(i==0){
            QString tempo = this->ultimas_leituras_sensores.at(i).at(7);
            QDateTime momento = QDateTime::fromString(tempo,"HH:mm:ss:zzz");

            tempo_inicial = momento.toSecsSinceEpoch();
            QString v = ultimas_leituras_sensores.at(i).at(1);
            velocidade_inicial =  v.toInt();
        }
        if(i==ultimas_leituras_sensores.size() -1)
        {
            QString t = this->ultimas_leituras_sensores.at(i).at(7);
            QDateTime momento = QDateTime::fromString(t,"HH:mm:ss:zzz");

            tempo_final = momento.toSecsSinceEpoch();
            QString v = this->ultimas_leituras_sensores.at(i).at(1);
            velocidade_final = v.toInt();
        }

    }
    float Delta_V = velocidade_inicial - velocidade_final;
    float Delta_T = tempo_inicial - tempo_final;
    aceleracaoMedia = Delta_V / Delta_T;

}

/**
 * @brief AtualizarLeiturasDeComandos - Método responsavel atualizar ultimas leituras de cada sensor e exibi-los na lateral superior esquerda
 * @param
 */
void MainWindow::AtualizarLeiturasDeComandos()
{
    QSqlQuery query("SELECT * from dados_sensores ORDER BY ds_id DESC LIMIT 1");
    while (query.next()) {
        this->velocidadeMomento =  query.value(POSICAO_VELOCIDADE_DB).toInt();
        this->umidadeMomento = query.value(POSICAO_UMIDADE_DB).toInt();
        this->temperaturaMomento = query.value(POSICAO_UMIDADE_DB).toInt();
        this->n_voltas = query.value(POSICAO_VOLTAS_DB).toInt();
        this->statusMomento = query.value(POSICAO_STATUSVEICULO_DB).toString();
        this->nivel_monoxido = query.value(POSICAO_MONOXIDO_DB).toInt();
        this->contagem_freios = query.value(POSICAO_FREIADAS_DB).toInt();
        ui->label_dataUltimaAtualizacao->setText(query.value(POSICAO_DATA_ATUALIZADA_DB).toDateTime().toString("dd/MM/yyyy HH:mm:ss"));
        this->ledVerde = query.value(POSICAO_ESTADO_LED_VERDE_DB).toInt();
        this->ledAmarelo = query.value(POSICAO_ESTADO_LED_AMARELO_DB).toInt();
        this->ledVermelho = query.value(POSICAO_ESTADO_LED_VERMELHO_DB).toInt();
    }

    StatusVeiculo();
    CalcularAceleracao();
    StatusTemperaturaMotor();
    StatusUmidade();
    StatusVelocidade();
    StatusAceleracao();
    StatusMonoxido();
    ui->lcdVelocidadeDisplay->display(this->velocidadeMomento);
    ui->lcdUmidadeDisplay->display(this->umidadeMomento);
    ui->lcdTemperaturaMotorDisplay->display(this->temperaturaMomento);
    ui->lcdMonoxidoDisplay->display(this->nivel_monoxido);
    ui->lcdContagemFreiadas->display(this->contagem_freios);
    ui->lcdDisplayNVoltas->display(this->n_voltas);
    QString format_a;
    ui->lcdAceleracaoMediaDisplay->display(format_a.sprintf("%+06.3f", this->aceleracaoMedia));

}

void MainWindow::on_btnSair_clicked()
{
    //depois sera fechado todas as conexoes antes de sair
    exit(-1);
}

void MainWindow::on_toolButton_clicked()
{

    QMessageBox::information(
        this,
        tr("Equipe Predadores - UTP"),
        tr("Interface Telemetria desenvolvida por: \n Alan Azevedo Bancks") );
}

/**
 * @brief VerificarEstadosLeds - Método responsavel por verificar retorno dos estados de led que estao no painel do piloto
 * @param
 */
void MainWindow::VerificarEstadosLeds()
{
    if(this->ledVerde != 0)
        ui->label_ledVerde->setPixmap(verde);
    else
        ui->label_ledVerde->setPixmap(desligado);


    if(this->ledAmarelo != 0)
        ui->label_ledAmarelo->setPixmap(amarelo);
    else
        ui->label_ledAmarelo->setPixmap(desligado);


    if(this->ledVermelho != 0)
        ui->label_ledVermelho->setPixmap(vermelho);
    else
        ui->label_ledVermelho->setPixmap(desligado);

}

void MainWindow::AtualizarDisplay()
{


//    while(1){

//#ifdef Q_OS_WIN
//        Sleep(uint(5000));
//#endif

//#ifdef Q_OS_UNIX
//        sleep(5);
//#endif


//        ui->btnAtualizarUltimosComandos->click();


//    }
}
