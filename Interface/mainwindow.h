#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QtCharts>
#include <QVector>
#include "dados.h"
#include "contantes_globais.h"
#include "databaseconexao.h"

#include <iostream>
#include <iomanip>


#ifdef Q_OS_WIN
#include <windows.h> // for Sleep
#endif

#ifdef Q_OS_UNIX
#include <unistd.h>
#endif

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( Dados &dadosTela, QWidget *parent = 0 );
    ~MainWindow();

    void StatusVelocidade();
    void StatusAceleracao();
    void StatusVeiculo();
    void StatusUmidade();
    void StatusMonoxido();
    void StatusTemperaturaMotor();
    void StatusQualidadeSinal();
    void AtualizandoDados(Dados*);
    void ExibirGraficoDeVelocidade();
    void ExibirGraficoDeTemperatura();
    void ExibirGraficoDeUmidade();
    void ExibirGraficoDeMonoxidoCarbono();
    void EnviaComandos(QString cmd);
    void AtualizarValoresGraficos();
    void CalcularAceleracao();

    void AtualizarGraficosVelocidade();
    void AtualizarGraficosTemperatura();
    void AtualizarGraficosUmidade();
    void AtualizarGraficosMonoxido();
    void AtualizarLeiturasDeComandos();
    void VerificarEstadosLeds();
    void AtualizarDisplay();

    int velocidadeMomento=0;
    int temperaturaMomento = 0;
    int umidadeMomento = 0;
    int n_voltas;
    int nivel_monoxido = 0;
    int contagem_freios = 0;
    float aceleracaoMedia = 0;
    int ledVerde = 0;
    int ledAmarelo = 0;
    int ledVermelho = 0;

    QPixmap verde = QPixmap(":logo/led_verde.png");
    QPixmap amarelo = QPixmap(":logo/led_amarelo.png");
    QPixmap vermelho = QPixmap(":logo/led_vermelho.png");
    QPixmap desligado = QPixmap(":logo/led_desligado.png");


    int MaXVelocidade = MAXIMO_VELOCIDADE;
    int MaxTemperatura  = MAXIMO_TEMPERATURA_EXTERNA;
    int MaxUmidade = MAXIMO_UMIDADE;
    QString statusMomento;
    QVector<QStringList> ultimos_comandos;
    QVector<QStringList> ultimas_leituras_sensores;

    QPalette pColor = palette();
    QPalette pColor2 = palette();
    QPalette pColor3 = palette();



    //elementos de grafico
    QDateTimeAxis *axisXVelocidade;
    QDateTimeAxis *axisYVelocidade;
    QLineSeries *seriesVelocidade;
    QChart *chartVelocidade;
    QChartView *chartViewVelocidade ;
    QMainWindow *viewWidgetVelocidade;


    QDateTimeAxis *axisXTemperatura;
    QDateTimeAxis *axisYTemperatura;
    QLineSeries *seriesTemperatura;
    QChart *chartTemperatura;
    QChartView *chartViewTemperatura ;
    QMainWindow *viewWidgetTemperatura;

    QDateTimeAxis *axisXUmidade;
    QDateTimeAxis *axisYUmidade;
    QLineSeries *seriesUmidade;
    QChart *chartUmidade;
    QChartView *chartViewUmidade ;
    QMainWindow *viewWidgetUmidade;

    QDateTimeAxis *axisXMonoxido;
    QDateTimeAxis *axisYMonoxido;
    QLineSeries *seriesMonoxido;
    QChart *chartMonoxido;
    QChartView *chartViewMonoxido;
    QMainWindow *viewWidgetMonoxido;

private slots:



    void on_BtnLigaVeiculo_clicked();

    void on_BtnDesligarCarro_clicked();

    void on_BtnSinalAmarelo_clicked();

    void on_BtnSinalVerde_clicked();

    void on_btnSinalVermelho_clicked();

    void on_BtnEnviaMenssagem_clicked();


    void on_btnAtualizarUltimosComandos_clicked();

    void on_btnSair_clicked();

    void on_toolButton_clicked();

private:
    Ui::MainWindow *ui;
    DataBaseConexao *conexaodb;


};

#endif // MAINWINDOW_H
