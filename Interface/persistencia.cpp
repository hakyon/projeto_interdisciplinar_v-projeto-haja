#include "persistencia.h"
#include "mainwindow.h"

#ifdef Q_OS_WIN
#include <windows.h> // for Sleep
#endif

#ifdef Q_OS_UNIX
#include <unistd.h>
#endif

/**
 * @brief Persistencia::Persistencia - Responsavel por permitir a consulta do banco em tempo real usando uma Thread
 * @param t
 * @param parent
 */

Persistencia::Persistencia(qint32 t,QObject *parent) : QObject(parent)
{
    myTime=t;
}

/**
 * @brief Persistencia::start - inicializa a Thread
 */
void Persistencia::start()
{
    timer = new QTimer();
    timer->start(myTime);
    qDebug()<<QString("startou o servico em:%1").arg(myTime);
    connect(timer,SIGNAL(timeout()),this,SLOT(doWork()));
}
/**
 * @brief Persistencia::doWork - slot do connect acima , efetua a consulta do banco e inicializa a tela apartir daqui
 */
void Persistencia::doWork()
{
    Dados *dados;
    dados = new Dados();

    qDebug()<<"Executando o prossesso paralelo";
    MainWindow *dadosAtuais;
    dadosAtuais = new MainWindow(*dados);
    dadosAtuais->setWindowTitle("Diagnostico - Veiculo");

    dadosAtuais->show();
    dadosAtuais->ExibirGraficoDeVelocidade();
    dadosAtuais->ExibirGraficoDeTemperatura();
    dadosAtuais->ExibirGraficoDeUmidade();
    dadosAtuais->ExibirGraficoDeMonoxidoCarbono();

    while(conexaoEstabelecida){

        // essa tratativa abaixo serve tratar as diferenças entre plataforma Linux/Windows
#ifdef Q_OS_WIN
        Sleep(uint(1000));
#endif

#ifdef Q_OS_UNIX
        sleep(1);
#endif

        QSqlQuery query("SELECT * from dadosensores ORDER BY id DESC LIMIT 1");
        while (query.next()) {
            dados->setVelocidade(query.value(1).toInt());
            dados->setUmidade(query.value(2).toInt());
            dados->setTemperatura(query.value(3).toInt());
            dados->setN_voltas(query.value(4).toInt());
            dados->setStatusDoVeiculo(query.value(5).toString());
            dados->setDataHora_UltimaAtualizacao(query.value(6).toDateTime().toString("dd/MM/yyyy HH:mm:ss"));
        }

        dadosAtuais->AtualizandoDados(dados);
    }
    disconnect();

    timer->stop();
    emit workFinished();
}

void Persistencia::InterrupcaoBotao()
{
    this->conexaoEstabelecida = 0;
}
