#ifndef PERSISTENCIA_H
#define PERSISTENCIA_H

#include <QThread>
#include <QTimer>
#include <QDebug>
#include <iostream>

#include "dados.h"
#include "databaseconexao.h"

using namespace std;

class Persistencia : public QObject
{
    Q_OBJECT

public:
    explicit Persistencia(qint32,QObject *parent = 0);

    qint32 myTime;
    int conexaoEstabelecida = 1;

signals:
    void   workFinished();

public slots:
    void doWork();
    void start();
    void InterrupcaoBotao();
private:
    QTimer *timer;

};

#endif // PERSISTENCIA_H
