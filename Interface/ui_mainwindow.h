/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabGrafico;
    QWidget *tabVelocidade;
    QGridLayout *gridLayout_6;
    QWidget *tabTemperatura;
    QGridLayout *gridLayout_5;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_7;
    QLabel *label_12;
    QPushButton *btnAtualizarUltimosComandos;
    QTableWidget *tableComados;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout;
    QLCDNumber *lcdVelocidadeDisplay;
    QLabel *label_4;
    QLabel *labelVelocidade;
    QLabel *labelUmidade;
    QProgressBar *limiteUmidade;
    QProgressBar *limiteVelocidade;
    QLCDNumber *lcdUmidadeDisplay;
    QLabel *label_5;
    QLCDNumber *lcdMonoxidoDisplay;
    QLabel *labelMonoxido;
    QLabel *label_6;
    QLabel *labelTemperatura;
    QProgressBar *limiteTemperatura;
    QLCDNumber *lcdTemperaturaMotorDisplay;
    QLabel *label_15;
    QLabel *label_16;
    QProgressBar *limiteCO;
    QLabel *labelAceleracao;
    QLCDNumber *lcdAceleracaoMediaDisplay;
    QProgressBar *limiteAceleracao;
    QProgressBar *limiteTemperaturaMotor;
    QLabel *labelTemperaturaMotor;
    QLCDNumber *lcdNumber;
    QLabel *label_13;
    QWidget *layoutWidget2;
    QGridLayout *gridLayout_2;
    QLabel *label_dataUltimaAtualizacao;
    QLabel *label_11;
    QLCDNumber *lcdDisplayNVoltas;
    QLabel *label_7;
    QLabel *label_10;
    QLabel *label_8;
    QLabel *labelMotorista;
    QLabel *label_9;
    QLabel *labelStatusVeiculo;
    QLabel *labelQualidadeSinal;
    QLCDNumber *lcdContagemFreiadas;
    QLabel *label_2;
    QLabel *label;
    QToolButton *toolButton;
    QWidget *layoutWidget3;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QLabel *label_ledVerde;
    QLabel *label_ledAmarelo;
    QLabel *label_ledVermelho;
    QSpacerItem *horizontalSpacer;
    QPushButton *BtnEnviaMenssagem;
    QLineEdit *lineEditCustomMessage;
    QPushButton *BtnLigaVeiculo;
    QPushButton *BtnSinalAmarelo;
    QPushButton *BtnDesligarCarro;
    QPushButton *BtnSinalVerde;
    QPushButton *btnSair;
    QPushButton *btnSinalVermelho;
    QLabel *label_enviocomandos;
    QLabel *label_3;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1150, 714);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabGrafico = new QTabWidget(centralWidget);
        tabGrafico->setObjectName(QStringLiteral("tabGrafico"));
        tabGrafico->setGeometry(QRect(390, 10, 731, 271));
        tabVelocidade = new QWidget();
        tabVelocidade->setObjectName(QStringLiteral("tabVelocidade"));
        gridLayout_6 = new QGridLayout(tabVelocidade);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        tabGrafico->addTab(tabVelocidade, QString());
        tabTemperatura = new QWidget();
        tabTemperatura->setObjectName(QStringLiteral("tabTemperatura"));
        gridLayout_5 = new QGridLayout(tabTemperatura);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        tabGrafico->addTab(tabTemperatura, QString());
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(391, 310, 601, 311));
        gridLayout_7 = new QGridLayout(layoutWidget);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_7->setContentsMargins(0, 0, 0, 0);
        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_7->addWidget(label_12, 0, 0, 1, 1);

        btnAtualizarUltimosComandos = new QPushButton(layoutWidget);
        btnAtualizarUltimosComandos->setObjectName(QStringLiteral("btnAtualizarUltimosComandos"));

        gridLayout_7->addWidget(btnAtualizarUltimosComandos, 0, 1, 1, 1);

        tableComados = new QTableWidget(layoutWidget);
        tableComados->setObjectName(QStringLiteral("tableComados"));

        gridLayout_7->addWidget(tableComados, 1, 0, 1, 2);

        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 12, 351, 170));
        gridLayout = new QGridLayout(layoutWidget1);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lcdVelocidadeDisplay = new QLCDNumber(layoutWidget1);
        lcdVelocidadeDisplay->setObjectName(QStringLiteral("lcdVelocidadeDisplay"));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 85, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(127, 170, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(63, 127, 255, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(0, 42, 127, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(0, 56, 170, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        QBrush brush7(QColor(255, 255, 220, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdVelocidadeDisplay->setPalette(palette);

        gridLayout->addWidget(lcdVelocidadeDisplay, 0, 1, 1, 1);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 0, 2, 1, 1);

        labelVelocidade = new QLabel(layoutWidget1);
        labelVelocidade->setObjectName(QStringLiteral("labelVelocidade"));

        gridLayout->addWidget(labelVelocidade, 0, 0, 1, 1);

        labelUmidade = new QLabel(layoutWidget1);
        labelUmidade->setObjectName(QStringLiteral("labelUmidade"));

        gridLayout->addWidget(labelUmidade, 1, 0, 1, 1);

        limiteUmidade = new QProgressBar(layoutWidget1);
        limiteUmidade->setObjectName(QStringLiteral("limiteUmidade"));
        limiteUmidade->setValue(10);

        gridLayout->addWidget(limiteUmidade, 1, 3, 1, 1);

        limiteVelocidade = new QProgressBar(layoutWidget1);
        limiteVelocidade->setObjectName(QStringLiteral("limiteVelocidade"));
        limiteVelocidade->setValue(10);

        gridLayout->addWidget(limiteVelocidade, 0, 3, 1, 1);

        lcdUmidadeDisplay = new QLCDNumber(layoutWidget1);
        lcdUmidadeDisplay->setObjectName(QStringLiteral("lcdUmidadeDisplay"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdUmidadeDisplay->setPalette(palette1);

        gridLayout->addWidget(lcdUmidadeDisplay, 1, 1, 1, 1);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 1, 2, 1, 1);

        lcdMonoxidoDisplay = new QLCDNumber(layoutWidget1);
        lcdMonoxidoDisplay->setObjectName(QStringLiteral("lcdMonoxidoDisplay"));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette2.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette2.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette2.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette2.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette2.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette2.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette2.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette2.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdMonoxidoDisplay->setPalette(palette2);

        gridLayout->addWidget(lcdMonoxidoDisplay, 4, 1, 1, 1);

        labelMonoxido = new QLabel(layoutWidget1);
        labelMonoxido->setObjectName(QStringLiteral("labelMonoxido"));

        gridLayout->addWidget(labelMonoxido, 4, 0, 1, 1);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 2, 2, 1, 1);

        labelTemperatura = new QLabel(layoutWidget1);
        labelTemperatura->setObjectName(QStringLiteral("labelTemperatura"));

        gridLayout->addWidget(labelTemperatura, 2, 0, 1, 1);

        limiteTemperatura = new QProgressBar(layoutWidget1);
        limiteTemperatura->setObjectName(QStringLiteral("limiteTemperatura"));
        limiteTemperatura->setValue(10);

        gridLayout->addWidget(limiteTemperatura, 2, 3, 1, 1);

        lcdTemperaturaMotorDisplay = new QLCDNumber(layoutWidget1);
        lcdTemperaturaMotorDisplay->setObjectName(QStringLiteral("lcdTemperaturaMotorDisplay"));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette3.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette3.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush);
        palette3.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette3.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette3.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette3.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette3.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette3.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette3.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette3.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette3.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdTemperaturaMotorDisplay->setPalette(palette3);

        gridLayout->addWidget(lcdTemperaturaMotorDisplay, 2, 1, 1, 1);

        label_15 = new QLabel(layoutWidget1);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout->addWidget(label_15, 4, 2, 1, 1);

        label_16 = new QLabel(layoutWidget1);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout->addWidget(label_16, 5, 2, 1, 1);

        limiteCO = new QProgressBar(layoutWidget1);
        limiteCO->setObjectName(QStringLiteral("limiteCO"));
        limiteCO->setValue(24);

        gridLayout->addWidget(limiteCO, 4, 3, 1, 1);

        labelAceleracao = new QLabel(layoutWidget1);
        labelAceleracao->setObjectName(QStringLiteral("labelAceleracao"));

        gridLayout->addWidget(labelAceleracao, 5, 0, 1, 1);

        lcdAceleracaoMediaDisplay = new QLCDNumber(layoutWidget1);
        lcdAceleracaoMediaDisplay->setObjectName(QStringLiteral("lcdAceleracaoMediaDisplay"));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette4.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette4.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush);
        palette4.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette4.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette4.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette4.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette4.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette4.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette4.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette4.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette4.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdAceleracaoMediaDisplay->setPalette(palette4);

        gridLayout->addWidget(lcdAceleracaoMediaDisplay, 5, 1, 1, 1);

        limiteAceleracao = new QProgressBar(layoutWidget1);
        limiteAceleracao->setObjectName(QStringLiteral("limiteAceleracao"));
        limiteAceleracao->setValue(24);

        gridLayout->addWidget(limiteAceleracao, 5, 3, 1, 1);

        limiteTemperaturaMotor = new QProgressBar(layoutWidget1);
        limiteTemperaturaMotor->setObjectName(QStringLiteral("limiteTemperaturaMotor"));
        limiteTemperaturaMotor->setValue(24);

        gridLayout->addWidget(limiteTemperaturaMotor, 3, 3, 1, 1);

        labelTemperaturaMotor = new QLabel(layoutWidget1);
        labelTemperaturaMotor->setObjectName(QStringLiteral("labelTemperaturaMotor"));

        gridLayout->addWidget(labelTemperaturaMotor, 3, 0, 1, 1);

        lcdNumber = new QLCDNumber(layoutWidget1);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette5.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette5.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette5.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush);
        palette5.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette5.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette5.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette5.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette5.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette5.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette5.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdNumber->setPalette(palette5);

        gridLayout->addWidget(lcdNumber, 3, 1, 1, 1);

        label_13 = new QLabel(layoutWidget1);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout->addWidget(label_13, 3, 2, 1, 1);

        layoutWidget2 = new QWidget(centralWidget);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 190, 351, 130));
        gridLayout_2 = new QGridLayout(layoutWidget2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_dataUltimaAtualizacao = new QLabel(layoutWidget2);
        label_dataUltimaAtualizacao->setObjectName(QStringLiteral("label_dataUltimaAtualizacao"));

        gridLayout_2->addWidget(label_dataUltimaAtualizacao, 0, 1, 1, 1);

        label_11 = new QLabel(layoutWidget2);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_2->addWidget(label_11, 0, 0, 1, 1);

        lcdDisplayNVoltas = new QLCDNumber(layoutWidget2);
        lcdDisplayNVoltas->setObjectName(QStringLiteral("lcdDisplayNVoltas"));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette6.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette6.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette6.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette6.setBrush(QPalette::Active, QPalette::Text, brush);
        palette6.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette6.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette6.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette6.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette6.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette6.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette6.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette6.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette6.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette6.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette6.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette6.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette6.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette6.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette6.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdDisplayNVoltas->setPalette(palette6);

        gridLayout_2->addWidget(lcdDisplayNVoltas, 1, 1, 1, 1);

        label_7 = new QLabel(layoutWidget2);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_2->addWidget(label_7, 1, 0, 1, 1);

        label_10 = new QLabel(layoutWidget2);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_2->addWidget(label_10, 5, 0, 1, 1);

        label_8 = new QLabel(layoutWidget2);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_2->addWidget(label_8, 3, 0, 1, 1);

        labelMotorista = new QLabel(layoutWidget2);
        labelMotorista->setObjectName(QStringLiteral("labelMotorista"));

        gridLayout_2->addWidget(labelMotorista, 5, 1, 1, 1);

        label_9 = new QLabel(layoutWidget2);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_2->addWidget(label_9, 4, 0, 1, 1);

        labelStatusVeiculo = new QLabel(layoutWidget2);
        labelStatusVeiculo->setObjectName(QStringLiteral("labelStatusVeiculo"));

        gridLayout_2->addWidget(labelStatusVeiculo, 4, 1, 1, 1);

        labelQualidadeSinal = new QLabel(layoutWidget2);
        labelQualidadeSinal->setObjectName(QStringLiteral("labelQualidadeSinal"));

        gridLayout_2->addWidget(labelQualidadeSinal, 3, 1, 1, 1);

        lcdContagemFreiadas = new QLCDNumber(layoutWidget2);
        lcdContagemFreiadas->setObjectName(QStringLiteral("lcdContagemFreiadas"));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette7.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette7.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette7.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette7.setBrush(QPalette::Active, QPalette::Text, brush);
        palette7.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette7.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette7.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette7.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette7.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette7.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette7.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette7.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette7.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette7.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette7.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette7.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette7.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette7.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette7.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette7.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette7.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette7.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette7.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette7.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette7.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette7.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lcdContagemFreiadas->setPalette(palette7);

        gridLayout_2->addWidget(lcdContagemFreiadas, 2, 1, 1, 1);

        label_2 = new QLabel(layoutWidget2);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 2, 0, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(110, 500, 151, 171));
        label->setPixmap(QPixmap(QString::fromUtf8(":/logo/logo_predador.png")));
        toolButton = new QToolButton(centralWidget);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setGeometry(QRect(1050, 0, 101, 20));
        layoutWidget3 = new QWidget(centralWidget);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(40, 340, 271, 163));
        gridLayout_4 = new QGridLayout(layoutWidget3);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_ledVerde = new QLabel(layoutWidget3);
        label_ledVerde->setObjectName(QStringLiteral("label_ledVerde"));
        label_ledVerde->setPixmap(QPixmap(QString::fromUtf8(":/logo/led_desligado.png")));

        gridLayout_3->addWidget(label_ledVerde, 0, 0, 1, 1);

        label_ledAmarelo = new QLabel(layoutWidget3);
        label_ledAmarelo->setObjectName(QStringLiteral("label_ledAmarelo"));
        label_ledAmarelo->setPixmap(QPixmap(QString::fromUtf8(":/logo/led_desligado.png")));

        gridLayout_3->addWidget(label_ledAmarelo, 0, 1, 1, 1);

        label_ledVermelho = new QLabel(layoutWidget3);
        label_ledVermelho->setObjectName(QStringLiteral("label_ledVermelho"));
        label_ledVermelho->setPixmap(QPixmap(QString::fromUtf8(":/logo/led_desligado.png")));

        gridLayout_3->addWidget(label_ledVermelho, 0, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 0, 3, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 1, 1, 1, 1);

        BtnEnviaMenssagem = new QPushButton(layoutWidget3);
        BtnEnviaMenssagem->setObjectName(QStringLiteral("BtnEnviaMenssagem"));

        gridLayout_4->addWidget(BtnEnviaMenssagem, 2, 0, 1, 1);

        lineEditCustomMessage = new QLineEdit(layoutWidget3);
        lineEditCustomMessage->setObjectName(QStringLiteral("lineEditCustomMessage"));

        gridLayout_4->addWidget(lineEditCustomMessage, 2, 1, 1, 1);

        BtnLigaVeiculo = new QPushButton(layoutWidget3);
        BtnLigaVeiculo->setObjectName(QStringLiteral("BtnLigaVeiculo"));

        gridLayout_4->addWidget(BtnLigaVeiculo, 3, 0, 1, 1);

        BtnSinalAmarelo = new QPushButton(layoutWidget3);
        BtnSinalAmarelo->setObjectName(QStringLiteral("BtnSinalAmarelo"));

        gridLayout_4->addWidget(BtnSinalAmarelo, 3, 1, 1, 1);

        BtnDesligarCarro = new QPushButton(layoutWidget3);
        BtnDesligarCarro->setObjectName(QStringLiteral("BtnDesligarCarro"));

        gridLayout_4->addWidget(BtnDesligarCarro, 4, 0, 1, 1);

        BtnSinalVerde = new QPushButton(layoutWidget3);
        BtnSinalVerde->setObjectName(QStringLiteral("BtnSinalVerde"));

        gridLayout_4->addWidget(BtnSinalVerde, 4, 1, 1, 1);

        btnSair = new QPushButton(layoutWidget3);
        btnSair->setObjectName(QStringLiteral("btnSair"));

        gridLayout_4->addWidget(btnSair, 5, 0, 1, 1);

        btnSinalVermelho = new QPushButton(layoutWidget3);
        btnSinalVermelho->setObjectName(QStringLiteral("btnSinalVermelho"));

        gridLayout_4->addWidget(btnSinalVermelho, 5, 1, 1, 1);

        label_enviocomandos = new QLabel(layoutWidget3);
        label_enviocomandos->setObjectName(QStringLiteral("label_enviocomandos"));

        gridLayout_4->addWidget(label_enviocomandos, 0, 0, 1, 2);

        label_3 = new QLabel(layoutWidget3);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_4->addWidget(label_3, 1, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1150, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabGrafico->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        tabGrafico->setTabText(tabGrafico->indexOf(tabVelocidade), QApplication::translate("MainWindow", "V", Q_NULLPTR));
        tabGrafico->setTabText(tabGrafico->indexOf(tabTemperatura), QString());
        label_12->setText(QApplication::translate("MainWindow", "Status de Comandos enviados", Q_NULLPTR));
        btnAtualizarUltimosComandos->setText(QApplication::translate("MainWindow", "Atualizar Ultimos comandos", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Limite", Q_NULLPTR));
        labelVelocidade->setText(QApplication::translate("MainWindow", "Velocidade :", Q_NULLPTR));
        labelUmidade->setText(QApplication::translate("MainWindow", "Umidade :", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Limite", Q_NULLPTR));
        labelMonoxido->setText(QApplication::translate("MainWindow", "CO", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Limite", Q_NULLPTR));
        labelTemperatura->setText(QApplication::translate("MainWindow", "Temperatura :", Q_NULLPTR));
        label_15->setText(QApplication::translate("MainWindow", "Limite", Q_NULLPTR));
        label_16->setText(QApplication::translate("MainWindow", "Limite", Q_NULLPTR));
        labelAceleracao->setText(QApplication::translate("MainWindow", "Acelera\303\247\303\243o ~", Q_NULLPTR));
        labelTemperaturaMotor->setText(QApplication::translate("MainWindow", "Temp. Motor:", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "Limite", Q_NULLPTR));
        label_dataUltimaAtualizacao->setText(QString());
        label_11->setText(QApplication::translate("MainWindow", "Ultima Atualiza\303\247\303\243o :", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "Numero de voltas:", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "Motorista : ", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "Qualidade de sinal :", Q_NULLPTR));
        labelMotorista->setText(QString());
        label_9->setText(QApplication::translate("MainWindow", "Status do veiculo :", Q_NULLPTR));
        labelStatusVeiculo->setText(QString());
        labelQualidadeSinal->setText(QString());
        label_2->setText(QApplication::translate("MainWindow", "Contagem de freiadas :", Q_NULLPTR));
        label->setText(QString());
        toolButton->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
        label_ledVerde->setText(QString());
        label_ledAmarelo->setText(QString());
        label_ledVermelho->setText(QString());
        BtnEnviaMenssagem->setText(QApplication::translate("MainWindow", "Enviar Menssagem", Q_NULLPTR));
        BtnLigaVeiculo->setText(QApplication::translate("MainWindow", "Ligar Veiculo", Q_NULLPTR));
        BtnSinalAmarelo->setText(QApplication::translate("MainWindow", "Envia Sinal Amarelo", Q_NULLPTR));
        BtnDesligarCarro->setText(QApplication::translate("MainWindow", "Desligar Veiculo", Q_NULLPTR));
        BtnSinalVerde->setText(QApplication::translate("MainWindow", "Envia Sinal Verde", Q_NULLPTR));
        btnSair->setText(QApplication::translate("MainWindow", "Sair", Q_NULLPTR));
        btnSinalVermelho->setText(QApplication::translate("MainWindow", "Envia Sinal Vermelho", Q_NULLPTR));
        label_enviocomandos->setText(QApplication::translate("MainWindow", "Envio de Comandos", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Indicador:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
