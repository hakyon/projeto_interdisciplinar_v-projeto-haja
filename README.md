### Universidade Tuiuti do Parana ###
### Ci�ncia da Computa��o ###
### Disciplina de Projeto Interdiciplinar V - Projeto Haja ###

### Sistema de telem�tria veic�lar ###

* Periodo : 06/2017 - 12/2017
* Version 1.96
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: C++/Python/Arduino
* Framework : Qt
* Plataforma : Linux/Windows
* Banco de dados - PostgreSQL
* Hardware - Arduino UNO/MEGA , Placa RF APC220

### Projetos ###

* Interface para telemetria de dados
* Servi�o de recebimento de pacotes/Parser
* Interface Hardware - Leitura de sensores(Arduino) 

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
