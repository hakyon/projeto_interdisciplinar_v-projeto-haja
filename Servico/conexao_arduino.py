#Autor : Alan Azevedo Bancks
#Data de criacao: aproximamente 05/10/2017
#Projeto Interdisciplinar V - Projeto HAJA

import time
import serial
import sys
import psycopg2
import random
import time
import datetime
from time import gmtime,strftime,localtime
import numpy as np
import logging
import glob

import envio_comando
from envio_comando import enviando_comando
 
DEVICE='' #Porta COM que sera usada

#dados de conexao do banco
DATABASE = 'dados_veiculo'
HOST = 'localhost'
PORTA = 5435
USUARIO = 'postgres'
SENHA = ''

#gerando arquivo de log do parser
logger = logging.getLogger('eventos_comunicacao')
hdlr = logging.FileHandler('eventos_comunicacao.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

 
def leitura_dados_serial():
	"""
	name: InfoComSerial()
	description: estabelecer conexao com arduino por serial
	return: 
	"""

	print '\nObtendo informacoes sobre a comunicacao serial\n'
	# Iniciando conexao serial
	comport = serial.Serial(DEVICE, 9600, timeout=1)
	logger.info('Conexao aberta Serial')
	time.sleep(1.8) # Entre 1.5s a 2s
	print '\nStatus da Porta: %s ' % (comport.isOpen())
	print 'Device conectado: %s ' % (comport.name)
	print 'Dump da configuracao:\n %s ' % (comport)
	print '\n###############################################\n'
	
	VALOR_DA_SERIAL= comport.readline()
	
	if len(VALOR_DA_SERIAL) == 0: #senao ler nada na serial ele fica em loop esperando
		leitura_dados_serial()
		logger.error('Nada esta vindo na Serial')

	
	print '\nRetorno da serial: %s' % (VALOR_DA_SERIAL)

	print '\n'
	# Fechando conexao serial
	comport.close() # depois que estiver funcionando conexao serial, nao poderei mais fechar essa porta
	logger.info('Conexao fechada Serial')
	#por algum motivo na leitura da serial vem lixo no primeiro caracter, ai isso eh removido pra nao da problema
	leitura_filtrada = VALOR_DA_SERIAL
	if VALOR_DA_SERIAL[0] != '0':	
		leitura_filtrada = VALOR_DA_SERIAL[1:]
		logger.info('Removido Lixo do inicial da leitura da porta serial')

	if (leitura_filtrada[0] != '0') | (leitura_filtrada[1] != 'x'):
		leitura_filtrada = leitura_filtrada[1:]
		logger.info('Removido Lixo do inicial da leitura da porta serial')

	#chama a funcao para converter dados do novo pacote
	trata_pacote(leitura_filtrada)
	
def atualizando_database(novo_pacote):
	"""
	name: atualizando_database()
	description: funcao responsavel por manter atualizar as informacoes no banco de dados constantemente
    """

	#configura acesso ao banco de dados e tambem insere novos valores
    
	velocidade = novo_pacote[0]
	umidade = novo_pacote[1]
	temperatura = novo_pacote[2]
	nivel_co = novo_pacote[3]
	freiadas = novo_pacote[4]
	voltas = novo_pacote[5]
	estado_led_verde = novo_pacote[6]
	estado_led_amarelo = novo_pacote[7]
	estado_led_vermelho = novo_pacote[8]	
	

	if velocidade != 0:
		status = 'EM MOVIMENTO'
	else:
		status = 'PARADO'
	
	
	atualizacao = verifica_hora()
	logger.info('Conectado ao banco')
	
	#configuracao do banco de dados e efetua insert no banco 
	con = psycopg2.connect(host=HOST,port = PORTA , database=DATABASE,user=USUARIO, password=SENHA)
	cur = con.cursor()
	
	sql = """INSERT INTO dados_sensores(ds_velocidade,ds_umidade,ds_temperatura,ds_monoxido,ds_voltas,ds_ocorrencia_freio,ds_ultima_atualizacao,ds_status,ds_estado_led_verde,ds_estado_led_amarelo,ds_estado_led_vermelho)
	VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""

	
    #executando
    	cur.execute(sql,(velocidade,umidade,temperatura,nivel_co,voltas,freiadas,atualizacao,status,estado_led_verde,estado_led_amarelo,estado_led_vermelho))		 
    	con.commit()
	logger.info('Novos dados inseridos')
    #verificando a insercao
    	cur.execute('SELECT * from dados_sensores ORDER BY ds_id DESC LIMIT 1')
    	recset = cur.fetchall()

    	con.close() #fechando a conexao com o banco
	time.sleep(2) # setado dois segundos para nao ficar muito tempo enchendo o banco de dados

def trata_pacote(pack_hexa):
	""" 
	name: trata_pacote()
	description: faz a leitura do pacote lido da serial na comunicacao do arduino e prepara para ser manipulado
	os valores como inteiros
	param1: pack_hexa - array to tipo string contendo valores em hexa
	return: leituras_convertidas - uma lista com valores inteiros convertidos da leitura
	"""
	
	logger.info('Pacote recebido em HEXA : ' + str(pack_hexa))
	
	#pega o pacote com string de hexa e remove as virgulas e insere em uma lista
	leituras_convertidas = []    
	pack_modificado = []
	pack_modificado = pack_hexa.split(',') 
	
	# pega os valores da lista modificada e converte cada um para inteiro inserindo em outra lista
	for i in pack_modificado:
    		leituras_convertidas.append(int(i,16))
	
	#print(leituras_convertidas)
	logger.info('Leituras convertidas para inteiro: ' + str(leituras_convertidas))
	return atualizando_database(leituras_convertidas)

def teste_de_aplicacao():
	"""
	name: teste_de_aplicacao()
	description: serve somente para simular um teste de recebimeto de leituras recebidas do arduino
	"""

	#funcao feita somente para testar com valores randomicos
	velocidade = random.randrange (10,120,1)
	umidade = random.randrange(4,44,1)
	temperatura = random.randrange(12,43,1)
	freiadas = random.randrange(0,50,1)
	nivel_co = random.randrange(12,43,1)
	voltas = 1
	status = 1
	atualizacao = verifica_hora() # aqui pega em datetime
	estado_led_verde = random.randrange(0,1,1)
	estado_led_amarelo = random.randrange(0,1,1)
	estado_led_vermelho = random.randrange(0,1,1)

	print "#Data e hora local: {}".format(atualizacao)

    #converte o datetime em timestamp e armazena em uma variavel local
	dt = datetime.datetime.strptime(atualizacao, "%Y-%m-%d %H:%M:%S")
	time.mktime(dt.timetuple()) + (dt.microsecond / 1000000)
	data_stamp = time.mktime(dt.timetuple())


    #bloco pra verificar se a conversao de timestamp funcinou corretamente fazendo o retorno pra datatime
	data_test = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(data_stamp)))
	print "#DateTime convertido em timestamp: {}".format(int(data_stamp))

	#cria o pacote com dados convertidos em inteiro junto com o timestamp no final
	randomico_inteiro = (velocidade,umidade,temperatura,nivel_co,freiadas,voltas,int(data_stamp),status,estado_led_verde,estado_led_amarelo,estado_led_vermelho)
	print "#Array de inteiro ja unido com o timestap: {}".format(randomico_inteiro)
	
	#converete tudo em um array de haxadecimal funcionando como um pacote e 
	#simulando o que vira do arduino
	randomico_hexadecimal = ','.join([hex(i) for i in randomico_inteiro])
	print "#Array em hexa simulando o comportamento do que vira do arduino: {}".format(randomico_hexadecimal)
	print "#Simulacao finalizada \n"

	print(trata_pacote(randomico_hexadecimal))
	
	return
	
	
	
def serial_ports():
    """ Lista todas as portas
    lista as postas seriais disponiveis
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        #verificacao para maquinas Linux
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Plataforma nao suportada')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result
   
def verifica_hora():
	"""
	name: verifica_hora
	description: funcao responsavel por capturar hora do sistema em timestamp e converte-la para padrao data do banco
	"""

	#funcao para capturar hora corrente
	return strftime("%Y-%m-%d %H:%M:%S",localtime())


""" main """
if __name__ == '__main__':
	print(serial_ports())
	if not serial_ports():
		logger.error('Nao ha portas disponiveis')
		exit()

	portas_disponiveis = serial_ports()
	DEVICE = portas_disponiveis[-1] #verifca quais portas estao disponiveis e seta a ultima
	while(1):#esse loop vai ficar garantindo insercao constante
		if enviando_comando == False:		
			leitura_dados_serial()
			#teste_de_aplicacao()
		else:
			print "Aguardando envio de comandos"
	
