#servico de envio de menssgens para Arduino receptor
#Autor : Alan Azevedo Bancks
#Data de criacao: aproximamente 15/11/2017
#Projeto Interdisciplinar V - Projeto HAJA

import time
import serial
import sys
import psycopg2
import time
import datetime
from time import gmtime,strftime,localtime
import numpy as np
import glob
import logging

DEVICE=''

enviando_comando=False;

COMANDO_SINAL_VERDE = '0x0001'
COMANDO_SINAL_VERMELHO ='0x0010'
COMANDO_SINAL_AMARELO ='0x0011'
COMANDO_LIGAR ='0x0100'
COMANDO_DESLIGAR ='0x0101'



#dados de conexao do banco
DATABASE = 'dados_veiculo'
HOST = 'localhost'
PORTA = 5432
USUARIO = 'postgres'
SENHA = ''

#gerando arquivo de log do parser
logger = logging.getLogger('eventos_envio_comandos')
hdlr = logging.FileHandler('eventos_envio_comandos.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

def consulta_comandos():
	"""
	name: consulta_comandos()
	description: funcao responsavel por verificar comandos existentes na base para serem enviados pela serial
	"""
	atualizacao = verifica_hora()
	check = False
	#configuracao do banco de dados
	con = psycopg2.connect(host=HOST,port = PORTA , database=DATABASE,user=USUARIO, password=SENHA)
	cur = con.cursor()

	#verifica sempre o ultimo comando
	sql = """SELECT * from comandos ORDER BY id_comando DESC LIMIT 1"""
	cur.execute(sql)

	rows = cur.fetchall()

	id = rows[0][0] #id da tabela comando
	comando = rows[0][1] # no do comando enviado pela interface
	data_execucao = rows[0][3]	
	logger.info('Verificando existencia de novos comandos para enviar')

	if data_execucao == None:
		if comando =='LIGA_VEICULO':
			check = enviar_comandos_serial(COMANDO_LIGAR)
		elif comando == 'DESLIGA_VEICULO':
			check = enviar_comandos_serial(COMANDO_DESLIGAR)
		elif comando == 'SINAL_VERDE':
			check = enviar_comandos_serial(COMANDO_SINAL_VERDE)
		elif comando == 'SINAL_AMARELO':
			check = enviar_comandos_serial(COMANDO_SINAL_AMARELO)
		elif comando == 'SINAL_VERMELHO':
			check = enviar_comandos_serial(COMANDO_SINAL_VERMELHO)

	#com a confirmacao do envio do comando para serial ,eh feito atualizacao no banco
	if check:
		sql = """UPDATE comandos SET data_execucao = %s WHERE id_comando = %s """
		cur.execute(sql,(atualizacao,id))		 
		con.commit()
		logger.info('Comando executado e atualizado no banco')
	else:
		print ('aguardando comandos... \n')

	con.close()		
		

def verifica_hora():
	"""
	name: verifica_hora
	description: funcao responsavel por capturar hora do sistema em timestamp e converte-la para padrao data do banco
	"""

	#funcao para capturar hora corrente
	return strftime("%Y-%m-%d %H:%M:%S",localtime())
	
	
def serial_ports():
    """ Lista todas as portas
    lista as postas seriais disponiveis
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        #verificacao para maquinas Linux
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Plataforma nao suportada')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def enviar_comandos_serial(comando):
	"""
	name: enviar_comandos_serial()
	description: funcao responsavel por enviar pacote de comando pela serial
	"""
	enviando_comando=True
	print ('Obtendo informacoes sobre a comunicacao serial\n')
	comport = serial.Serial(DEVICE, 9600, timeout=1)
	time.sleep(1.8) 
	print 'Status da Porta: %s ' % (comport.isOpen())
	print 'Device conectado: %s ' % (comport.name)
	print 'Dump da configuracao:\n %s ' % (comport)
	comport.write(comando)

	print 'Comando Enviado: %s' % (comando)
	comport.close()
	enviando_comando=False
	return True

""" main """
if __name__ == '__main__':	
	print(serial_ports())
	if not serial_ports():
		logger.error('Nao ha portas disponiveis')
		exit()

	portas_disponiveis = serial_ports()
	DEVICE = portas_disponiveis[-1] #verifca quais portas estao disponiveis e seta a ultima
	while(1):#esse loop vai ficar garantindo insercao constante
		time.sleep(1)
		consulta_comandos()

