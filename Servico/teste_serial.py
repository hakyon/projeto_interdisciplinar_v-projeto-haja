import time
import serial
import sys
import psycopg2
import random
import time
import datetime
from time import gmtime,strftime,localtime
import numpy as np
 
DEVICE='/dev/ttyUSB0'
 
def leitura_dados_serial():
	"""
	name: InfoComSerial()
	description: estabelecer conexao com arduino por serial
	return: 
	"""

	print '\nObtendo informacoes sobre a comunicacao serial\n'
	# Iniciando conexao serial
	comport = serial.Serial(DEVICE, 9600, timeout=1)
	time.sleep(1.8) # Entre 1.5s a 2s
	print '\nStatus da Porta: %s ' % (comport.isOpen())
	#print '\nQualidade do sinal: %s ' % (comport.open())
	print 'Device conectado: %s ' % (comport.name)
	print 'Dump da configuracao:\n %s ' % (comport)
	print '\n###############################################\n'
	
	VALOR_DA_SERIAL= comport.readline()
	print '\nRetorno da serial: %s' % (VALOR_DA_SERIAL)

	print '\n'
	# Fechando conexao serial
	comport.close() # depois que estiver funcionando conexao serial, nao poderei mais fechar essa porta
	
	#por algum motivo na leitura da serial vem lixo no primeiro caracter, ai isso eh removido pra nao da problema
	leitura_filtrada = VALOR_DA_SERIAL
	#if VALOR_DA_SERIAL[0] != '0':	
	#	leitura_filtrada = VALOR_DA_SERIAL[1:]

	#if (leitura_filtrada[0] != '0') | (leitura_filtrada[1] != 'x'):
	#	leitura_filtrada = leitura_filtrada[1:]

""" main """
if __name__ == '__main__':
	#leitura_dados_serial() 
	#teste_de_aplicacao()
	while(1):#esse loop vai ficar garantindo insercao constante
		leitura_dados_serial()
	
